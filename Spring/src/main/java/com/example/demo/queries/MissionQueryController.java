package com.example.demo.queries;

import java.util.List;

import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.Candidat;
import com.example.demo.data.Mission;
import com.example.demo.data.Missionepository;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/mission")
@CrossOrigin(origins = "http://localhost:3000")
public class MissionQueryController {
	private final QueryGateway queryGateway;
	private final MissionQueryService m;
	@Autowired
	private Missionepository rep;

	public MissionQueryController(QueryGateway queryGateway,MissionQueryService m) {
		this.queryGateway = queryGateway;
		this.m= m;
	}

	@GetMapping("/getmission/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale')")
	public ResponseEntity<Mission> getCandidat(@PathVariable int id) {
		Mission mission = queryGateway.query(new FindMissionById(id), Mission.class).join();

		if (mission == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(mission, HttpStatus.OK);
	}
	
    @GetMapping("/missiontermine")
  //  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale') ")
    public List<Mission> getMissionTermine(){
        return this.m.getMissionTermine();
    }
    
    @GetMapping("/missionencour")
   // @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale') ")
    public List<Mission> getMissionEncour(){
        return this.m.getMissionEncour();
    }
    @GetMapping("/allmission")
   // @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale') ")
    public List<Mission> hetAllmission(){
        return this.rep.findAll();
    }
    
    @GetMapping("/missionnonaffecte")
   // @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale')")
    public List<Mission> getMissionnonaffecte(){
        return this.m.getMissionNonaffecte();
    }

}
