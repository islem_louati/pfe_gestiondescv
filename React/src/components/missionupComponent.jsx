import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import axios from "axios";
import Mission from "../services/Mission";
import PhoneInput from "react-phone-input-2";


const options = [
  { value: 'done', label: 'done' },
  { value: 'INNACTIF', label: 'INNACTIF' },

];
const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
const invalide = value => {
    if(! value.match(/^([a-zA-Z ]+)$/)) {
        return (
            <div className="alert alert-danger" role="alert">
                Pseudo invalide!
            </div>
        );
    }
};



const vusername = value => {
    if (value.length < 3 || value.length > 20 ) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class updatemiss extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            description: '',
            datedebut: '',
            datefin: '',
            etat: '',
            successful: false,
            message: "",
            dBFile:'',
        }
    }
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            Mission.getallmission(this.state.id).then( (res) =>{
                let mission = res.data;
                this.setState({description: mission.description,
                    datedebut: mission.datedebut,
                    datefin: mission.datefin,
                    etat: mission.etat,

                });
            });
        }
    }

    saveOrUpdateMission = (m) => {
        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();
        m.preventDefault();
        let missions = {description: this.state.description, datedebut: this.state.datedebut,datefin:this.state.datefin,etat: this.state.etat};
       

        // step 5

        if (this.checkBtn.context._errors.length === 0) {

            Mission.updtaeMission(missions,this.state.id
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                       
                    });
                   
                    this.props.history.push(`/missions`);
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        
        
        }}
   
    
    changeDescriptionHandler= (event) => {
        this.setState({description: event.target.value});
    }

    changedatedebutHandler= (event) => {
        this.setState({datedebut: event.target.value});
    }



    changedatefinHandler= (event) => {
        this.setState({datefin: event.target.value});
    }
    changeetatHandler= (event) => {
        this.setState({etat: event.target.value});
    }


    cancel(){
        this.props.history.push('/missions');
    }

    getTitle() {
        if (this.state.id === '_add') {
            return <h3 className="text-center">Add mission</h3>
        } else {
            return <h3 className="text-center">Update Mission</h3>
        }
    }

    render() {
        return (
        <body>
      <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    
    
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>



        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">login</i>
            </div>
            <span class="nav-link-text ms-1" >Profile</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/registre">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">assignment</i>
            </div>
            <span class="nav-link-text ms-1">Comment</span>
          </a>
        </li>
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
 
  </aside>
  <div className="col-md-12" style={{marginLeft: '18rem'}}>
<div class="main-body">
<div class="page-wrapper">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>modifier Candidat</h5>
                                        </div>
                                        <div class="card-body">


                                            <div class="row">
                                                <div class="col-md-6">
                              <Form
                                  onSubmit={this.saveOrUpdateMission}
                                  ref={c => {
                                      this.form = c;
                                  }}
                              >
                                  {!this.state.successful && (
                                      <div>
                                        
                         
                                
                                          <div className="form-group">
                              
                                               <label htmlFor="description">description </label>
                                              <Input
                                                  type="text" className="form-control" 
                                                  value={this.state.description} onChange={this.changeDescriptionHandler}
                                                  validations={[required,invalide,vusername]}
                                              />
                                          </div>

                                          <div className="form-group">
                                              <label for="date">datedebut </label>
                                              <Input
                                                  type="date" className="form-control" 
                                                  value={this.state.datedebut} onChange={this.changedatedebutHandler}
                                                  min="2021-01-01" max="2040-12-31"
                                                 
                                                  validations={[required]}

                                              />
                                              
                                          </div>
                                          <div className="form-group">
                                              <label for="date">date fin </label>
                                              <Input
                                                  type="date" className="form-control" 
                                                  value={this.state.datefin} onChange={this.changedatefinHandler}
                                                  min="2021-01-01" max="2040-12-31"
                                                 
                                                  validations={[required]}

                                              />
                                              
                                          </div>
                                          <div className="form-group">
                                               <label for="tel">tel </label>
                                              <PhoneInput
                                                 type="number" 
                                                 country={'fr'}
                                                  value={this.state.tel}
                                                  onChange={this.onChangePassword}
                                                  validations={[required]}
                                              />

                                          </div>
                                         
                                          <div className = "form-group">
                                        
                                          <select onChange={(ddl)=>{this.setState({etat:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--Etat--</option>
                                                <option value="NON_AFFECTE">NON_AFFECTE</option>
                                                <option value="EN_COUR">EN_COUR</option>
                                                <option value="TERMINE">TERMINE</option>
                                            </select>
                                    </div>


                                          <div className="form-group">
                                              <button className="btn btn-primary btn-block" onClick={this.saveOrUpdateMission} >ajouter</button>
                                          </div>
                                      </div>
                                  )}

                                  {this.state.message && (
                                      <div className="form-group">
                                          <div
                                              className={
                                                  this.state.successful
                                                      ? "alert alert-success"
                                                      : "alert alert-danger"
                                              }
                                              role="alert"
                                          >
                                              {this.state.message}
                                          </div>
                                      </div>
                                  )}
                                  <CheckButton
                                      style={{ display: "none" }}
                                      ref={c => {
                                          this.checkBtn = c;
                                      }}
                                  />
                              </Form>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                          </div>
                      </div>

</body>

        );
    }
}
export default updatemiss;
