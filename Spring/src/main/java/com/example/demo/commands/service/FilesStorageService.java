package com.example.demo.commands.service;

import org.springframework.stereotype.Component;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.FileDB;
import com.example.demo.data.FileDBRepository;
import com.example.demo.data.FileInfo;
import com.example.demo.data.Mission;
import com.example.demo.data.Missionepository;

@Component
public class FilesStorageService  {
	 private final Path root = Paths.get("uploads/files");

	    private static final String MP4_CONTENT_TYPE = "video/mp4";
	    private static final String DOCX_CONTENT_TYPE ="document/docx";
	 @Autowired
	  private FileDBRepository fileDBRepository;
	 @Autowired
	 private  CandidatRepository repository;
	 @Autowired
	 private  Missionepository rep;
	 

	  public void store(MultipartFile file,int CandidatId) throws IOException {
		  
		  try {
			
			
	    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
	    
	    FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());
	    
	            fileDBRepository.save(FileDB);

	    		Candidat candidat =	repository.findById(CandidatId).orElse(null);
	    		
	    		candidat.setdBFile(FileDB);
	    		repository.save(candidat);
	  }catch (MalformedURLException e) {
	      throw new RuntimeException("Error: " + e.getMessage());
	    }
		   }
	  public String allfiles(int missionId) throws IOException {
		  String l="islem";
		  Mission p = this.rep.findById(missionId).orElse(null);
		  p.getDescription();
		  System.out.println(p.getDescription());
		  boolean found = true;
		  File dir = new File("uploads/files");
	        File[] files = dir.listFiles();
	        FileDB f= new FileDB();
	        for(FileDB ff : fileDBRepository.findAll()) {
	        	
	        
	        // Fetching all the files
	        for (File file : files) {
	            if(file.isFile()) {
	            	
	            	if(file.getName().substring(0,file.getName().length()-4).equals(ff.getName()))
	            	{
	                BufferedReader inputStream = null;
	                String line;
	                try {
	                	 FileInputStream fis = new FileInputStream(file);		
	     				PDDocument pdfDocument = PDDocument.load(fis);
	     			//	System.out.println("Number of Pages: " +pdfDocument.getPages().getCount()+file.getName());
	     				

	     				PDFTextStripper pdfTextStripper = new PDFTextStripper();
	     				pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire document
	     				pdfTextStripper.getEndPage(); // comment this line if you want to read the entire document
	     				String docText = pdfTextStripper.getText(pdfDocument);
	     				
if (docText.contains( l)) {
	     				System.out.println(docText);
	     			
	     				System.out.println("Number of Pages: " +docText.getBytes().length+file.getName().substring(0,file.getName().length()-4));
	     				 
	     				System.out.println("database: " +ff.getName());
	     				
	     				return docText;
		     				

}
	                    
	                }catch(IOException e) {
	                	System.out.println(e);
	                }
	              
	            }}
	         
	        }}
			return l;
			
	        
		}
		  
	  public void init() {
	    try {
	      Files.createDirectory(root);
	    } catch (IOException e) {
	      throw new RuntimeException("Could not initialize folder for upload!");
	    }
	  }
	  
	  public void save(MultipartFile file) throws IOException {
		  String fileName = StringUtils.cleanPath(file.getOriginalFilename());
	      FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());
		  try {
	     
         
	      Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
	      //copy the file to the upload directory,it will replace any file with same name.
        //  Files.copy(file.getInputStream(),  this.root.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
	     } catch (Exception e) {

	      throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
	    }
	  }
	  
	  public Resource load(String filename) throws FileNotFoundException, IOException {
	    try {
	      Path file = root.resolve(filename);
	      Resource resource = new UrlResource(file.toUri());
	      if (resource.exists() || resource.isReadable()) {
	    	  FileInputStream fis = new FileInputStream(resource.getFile());		
				PDDocument pdfDocument = PDDocument.load(fis);

				System.out.println("Number of Pages: " +pdfDocument.getPages().getCount());

				PDFTextStripper pdfTextStripper = new PDFTextStripper();
				pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire document
				pdfTextStripper.setEndPage(3); // comment this line if you want to read the entire document
				String docText = pdfTextStripper.getText(pdfDocument);

				System.out.println(docText);
	        return resource;
	        
	      } else {
	        throw new RuntimeException("Could not read the file!");
	      }
	    } catch (MalformedURLException e) {
	      throw new RuntimeException("Error: " + e.getMessage());
	    }
	  }
	
	  
	  public void deleteAll() {
	    FileSystemUtils.deleteRecursively(root.toFile());
	  }
	  
	  public Stream<Path> loadAll() {
	    try {
	    	
	      return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
	    } catch (IOException e) {
	      throw new RuntimeException("Could not load the files!");
	    }
	  }
	}
