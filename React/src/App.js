import React, { Component } from "react";
import { Switch, Route, Link, Redirect } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardModerator from "./components/board-moderator.component";
import BoardAdmin from "./components/board-admin.component";

// import AuthVerify from "./common/auth-verify";
import EventBus from "./common/EventBus";
import ListCandidatComponent from "./components/ListCandidatComponent";
import Profilee from "./components/profile.component";
import CreateCandidat from "./components/CreateCandidatComponent";
import updateCandidat from "./components/updateComponent";
import UploadFiles from "./components/upload-files.component";
import CreateCommentComponent from "./components/CreateCommentComponent";
import ListClient from "./components/listClient";
import allMission from "./components/ListMissionTermine";
import CreateMissionComponent from "./components/CreateMissionComponent";
import updatemiss from "./components/missionupComponent";
import missionNonAffecter from "./components/missionNonTerminer";
import missionterminer from "./components/missionTerminer";
import missionEnCOUR from "./components/missionEnCoursComponent";
import CreateClient from "./components/CreateClient";
import CreateClientt from "./components/CreateLient";
import ListCommerciale from "./components/ListCommerciale";
import CreateCommercialeComponent from "./components/CreateCommerciale";
import usersComponent from "./components/ListUsers";
import Question from "./components/Quiz/Question/Question";
import Quiz from "./components/Quiz/Quiz";
import Createquestion from "./components/Quiz/Question/CreateQuestion";
import affectanswer from "./components/Quiz/Question/affectanswertoquestion";
import ListquestionComponent from "./components/Quiz/Question/ListQuestionforasmin";
import ListCandidatSimilaire from "./components/Candidatssimilaire";
import ListCommercialee from "./components/afficheComerciale";
import ListCandidatComponentforcomerciale from "./components/Comerciale/ListCandidatformcom";
import ListClientforComerciale from "./components/Comerciale/listClient";
import allMissionforcom from "./components/Comerciale/ListMissionTermine";
import missionNonAffecterforcom from "./components/Comerciale/missionNonTerminer";
import missionterminerforcom from "./components/Comerciale/missionTerminer";
import missionEnCOURforcom from "./components/Comerciale/missionEnCoursComponent";
import Createquestionforcom from "./components/Comerciale/Question/CreateQuestion";
import affectanswerforcom from "./components/Comerciale/Question/affectanswertoquestion";
import Registerr from "./components/registreComponent";
import homeClientt from "./components/frontcandidat/CreateCandidat";
import profilecomerciale from "./components/frontcandidat/profileComponent";
import addCandidat from "./components/frontcandidat/addCandidat";
import LProfileComponent from "./components/frontcandidat/profileComponent";
import affectCandidattoUser from "./components/frontcandidat/affectCandidatToUser";
import ChatRoom from "./components/login";
import CreateCommercialeByAdminComponent from "./components/createCommercialeByadmin";
import affectComecialetoUser from "./components/Comerciale/LoginforcommercialecreatedbyAdmin";
import affectCandidatcreatebyasmintoUser from "./components/frontcandidat/registrefromadmin";
import usersnonenableComponent from "./components/listUserNonEnable";
import rechercheCandidat from "./components/recherchecandidat";
import ContactList from "./components/recherchecandidat";
import LandingPage from "./components/recherchecandidat";
import recherche from "./components/recherchecandidat";
import pdfviewr from "./components/pdfviewr";
import ListClientsans from "./components/clientsanscom";
import ListClientavec from "./components/Clientavec";
import { object } from "yup";
import ListCvComponent from "./components/listcv";




class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      showUserBoard: false,
      currentUser: undefined,
      history:"",
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.appUserRole==="COMMERCIALE",
        showAdminBoard:  user.appUserRole==="ADMIN",
        showUserBoard:  user.appUserRole==="CANDIDAT",
      });
    }
    
    EventBus.on("logout", () => {
      this.logOut();
    });
  }

  componentWillUnmount() {
    EventBus.remove("logout");
  }

  logOut() {
    AuthService.logout();
    this.setState({
      showModeratorBoard: false,
      showAdminBoard: false,
      showUserBoard:false,
      currentUser: undefined,
    });
  }

  render() {
    const { currentUser, showUserBoard,showModeratorBoard, showAdminBoard } = this.state;




    return (
      <div>

<div class="onenav">
<nav class="navbar navbar-expand-lg blur border-radius-xl top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 onenav">
          <div >
        

            {showUserBoard && (
              <li className="nav-item">
               
               
              </li>
            )}


            {showAdminBoard && (
              
              
                <Link to={"/candidat"} className="nav-link">
                 
                </Link>
             
            )}

            {showModeratorBoard && (
              <li className="nav-item">
                <Link to={"/candidatcomerciale"} className="nav-link">
             
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  LogOut
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

   </div>

      
          <Switch>
            <Route exact path={["/", "/home"]} component={homeClientt} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Registerr} />
            <Route exact path="/profile" component={Profilee} />
            <Route exact path="/profilee" component={LProfileComponent} />
            <Route path="/candidat" component={ListCandidatComponent} />
            <Route path='/candidats/:id' component={ListCandidatSimilaire} />
            <Route path="/add" component={CreateCandidat}/>
            <Route path='/update/:id' component={updateCandidat}/>
            <Route path='/upload/:id' component={UploadFiles}/>
            <Route path='/comment/:id' component={CreateCommentComponent}/>
            <Route path="/missions" component={allMission}/>
            <Route path="/missionnonaffecter" component={missionNonAffecter}/>
            <Route path="/missionterminer" component={missionterminer}/>
            <Route path='/updatemission/:id' component={updatemiss}/> 
            <Route path="/mission" component={missionEnCOUR}/>  

            <Route path="/missionsimi/:id" component={ListCandidatSimilaire}/>  
            <Route path="/add-mission" component={CreateMissionComponent}/>
            <Route path="/client" component={ListClient}/>
            <Route path="/addclient" component={CreateClientt}/>

            <Route path="/commerciale/:id" component={ListCommerciale}/>
            <Route path="/commerciale" component={ListCommercialee}/>
            <Route path="/addcommerciale/:id" component={CreateCommercialeComponent}/>
            <Route path="/addcommercialebyadmin" component={CreateCommercialeByAdminComponent}/>

            <Route path="/user" component={usersComponent}/>
            <Route path="/admin" component={BoardAdmin} />
          
            <Route path="/candidatcomerciale" component={ListCandidatComponentforcomerciale}/> 
                <Route path="/quiz" component={Quiz}/>   
                <Route path="/createquiz" component={Createquestion}/>  
                <Route path="/affquiz/:id" component={affectanswer}/>  
                <Route path="/email/:id" component={affectanswer}/>  
                <Route path="/list" component={ListquestionComponent}/>

                            <Route path="/listnonenable" component={usersnonenableComponent}/>  


                            <Route path="/clientsans" component={ListClientsans}/>
                            <Route path="/clientavec" component={ListClientavec}/>

                
                            <Route path="/cc" component={pdfviewr}/>  
                            <Route path="/cv" component={ListCvComponent}/>  

                <Route path="/ClientForcom" component={ListClientforComerciale}/>
                <Route path="/missionsforcom" component={allMissionforcom}/>
                <Route path="/missionnonaffecterforcom" component={missionNonAffecterforcom}/>
            <Route path="/missionterminerforcom" component={missionterminerforcom}/> 
            <Route path="/missioencourforcom" component={missionEnCOURforcom}/>    

            
                <Route path="/createquizforcom" component={Createquestionforcom}/>  
                <Route path="/affquizforcom/:id" component={affectanswerforcom}/>    


                <Route path="/addcandidattouser/:id" component={affectCandidattoUser}/>
                <Route path="/commercialetouser/:id" component={affectComecialetoUser}/>
                <Route path="/candidatcreatedbyadmintouser/:id" component={affectCandidatcreatebyasmintoUser}/>

                      <Route path="/complete" component={addCandidat}/>     
                      <Route path="/chat" component={ChatRoom}/>        
          </Switch>
      

        { /*<AuthVerify logOut={this.logOut}/> */ }
      </div>
    );
  }
}

export default App;
