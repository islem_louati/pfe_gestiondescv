package com.example.demo.registration.token;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.example.demo.data.AppUserRole;
import com.example.demo.data.Candidat;
import com.example.demo.data.comerciale;

public class requestr {
    
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	@NotNull
    @Size(max=30)
	@Min(value=3)
	@NotEmpty(message = "entrer votre email")	
	private String username;
	   private Boolean enabled = true;
	@Email
	private String email;
	private String password;
    @Enumerated(EnumType.STRING)
    private AppUserRole appUserRole;
	private comerciale comerciale;
	private Candidat candidat;
	
	public Candidat getCandidat() {
		return candidat;
	}

	public Candidat setCandidat(Candidat candidat) {
		return this.candidat = candidat;
	}

	public requestr(Long id, @NotNull @Size(max = 30) @Min(3) @NotEmpty(message = "entrer votre email") String username,
			Boolean enabled, @Email String email, String password, AppUserRole appUserRole, Candidat candidat) {
		super();
		this.id = id;
		this.username = username;
		this.enabled = enabled;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.candidat = candidat;
	}

	public requestr(Long id,
			@NotNull @Size(max = 30) @Min(3) @NotEmpty(message = "entrer votre email") String username,
			@Email String email, String password, AppUserRole appUserRole,
			com.example.demo.data.comerciale comerciale) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.comerciale = comerciale;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public comerciale getComerciale() {
		return comerciale;
	}

	public com.example.demo.data.comerciale setComerciale(comerciale comerciale) {
		return this.comerciale = comerciale;
	}

	public AppUserRole getAppUserRole() {
		return appUserRole;
	}

	public void setAppUserRole(AppUserRole appUserRole) {
		this.appUserRole = appUserRole;
	}


	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public requestr(Boolean enabled, Long id,
			@NotNull @Size(max = 30) @Min(3) @NotEmpty(message = "entrer votre email") String username,
			@Email String email, String password, AppUserRole appUserRole,
			com.example.demo.data.comerciale comerciale) {
		super();
		this.enabled = enabled;
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.comerciale = comerciale;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public requestr() {
		super();
	}

	@Override
	public String toString() {
		return "RegistrationRequest [username=" + username + ", email=" + email
				+ ", password=" + password + ", appUserRole=" + appUserRole + "]";
	}


}

