import React, {Component, useState} from 'react';

import { Link } from 'react-router-dom';

import axios from 'axios';
import CandidatService from '../services/CandidatService';
import Mission from '../services/Mission';
import userService from '../services/user.service';
import authHeader from '../services/auth-header';
class ListCandidatComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
          
          id:this.props.match.params.id,

            candidats: [],
            missions:{},
            Profile: [],
           
        }
        this.addCandidat = this.addCandidat.bind(this);
        this.editCandidat = this.editCandidat.bind(this);
        this.deletecandidat=this.deletecandidat.bind(this);
    }
   
    viewCandidat(id){
   
      Mission.getmissionsById(this.state.id).then(res=>{
        this.setState({mission:res.data});
    console.log(res.data.missionId)
   
    axios.post(`http://localhost:8087/SpringMVC/candidats/affect/${res.data.missionId}/${id}`)
    .then((response) => {
      this.setState({ missions: response.data });
    })
   window.alert("candidat affecter")
      
  }) }
  updatecandidat(id) {
    {
      
        
      this.props.history.push(`/update/${id}`);
  }
    
}

deletecandidat(id){
  axios.delete(`http://localhost:8087/SpringMVC/candidats/delete/${id}`,{ headers: authHeader() })
 
  window.alert("confirmer la supression ?")
  console.log(id)
  window.location.reload();
}
exporterComment(id){
  //axios.get(`http://localhost:8087/SpringMVC/Comment/comm/${id}`,{ headers: authHeader() })


axios({
  url: `http://localhost:8087/SpringMVC/Comment/comm/${id}`, //your url
  method: 'GET',
  responseType: 'blob', // important
}).then((response) => {
   const url = window.URL.createObjectURL(new Blob([response.data]));
   const link = document.createElement('a');
   link.href = url;
   link.setAttribute('download', 'file.pdf'); //or any other extension
   document.body.appendChild(link);
   link.click();
});
}

    addCandidat(){
        this.props.history.push('/create/_add');
    }
    
    

    editCandidat(id){
        
      this.props.history.push(`/upload/${id}`);
  }

  
  affectComment(id){
     
    this.props.history.push(`/comment/${id}`);
}

componentDidMount() {
  userService.getAdminBoard().then((res) =>{
      this.setState(({Profile: res.data}));
      console.log(res.data)
      });

}
componentWillUnmount() {
document.removeEventListener('mousedown', this.getVal)
}
getVal = (e) => {
if (this.node.current.contains(e.target)) {
  return
}
this.setState({
  userList: [],
})
}
onChangee = async (e) => {
if (this.cancelToken) {
  this.cancelToken.cancel()
}
this.cancelToken = axios.CancelToken.source()
await axios
  .get(`http://localhost:8087/SpringMVC/candidat/candidats`,{ headers: authHeader() 
  })
  .then((res) => {
    this.setState({
      Profile: res.data,
    })
  })
  .catch((e) => {
    if (axios.isCancel(e) || e) {
      console.log('Data not found.')
    }
  })
let stringKwd = e.target.value.toLowerCase()
let filterData = this.state.Profile.filter((item) => {
  return item.adresse.toLowerCase().indexOf(stringKwd) !== -1
})
this.setState({
  Profile: filterData,
})
}
onChange = async (e) => {
if (this.cancelToken) {
  this.cancelToken.cancel()
}
this.cancelToken = axios.CancelToken.source()
await axios
  .get(`http://localhost:8087/SpringMVC/candidat/candidats`,{ headers: authHeader() 
  })
  .then((res) => {
    this.setState({
      Profile: res.data,
    })
  })
  .catch((e) => {
    if (axios.isCancel(e) || e) {
      console.log('Data not found.')
    }
  })
let stringKwd = e.target.value.toLowerCase()
let filterData = this.state.Profile.filter((item) => {
  return item.dBFile.name.toLowerCase().indexOf(stringKwd) !== -1 
})
this.setState({
  Profile: filterData,
})

}


    render() {
   
   

        return (
          <div>
              <body>
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    
    
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>



        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">login</i>
            </div>
            <span class="nav-link-text ms-1" >Profile</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/registre">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">assignment</i>
            </div>
            <span class="nav-link-text ms-1">Comment</span>
          </a>
        </li>
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
 
  </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps ps--active-y">
   
       <div class="container-fluid py-4">
       
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '-2073px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '2073px', height: '724px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '536px', height: '187px'}}></div></div>
    </main>
    <div class="fixed-plugin ps">
   
        <div class="card shadow-lg">
          <div class="card-header pb-0 pt-3">
            <div class="float-start">
              <h5 class="mt-3 mb-0">Material UI Configurator</h5>
              <p>See our dashboard options.</p>
            </div>
            <div class="float-end mt-4">
              <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                <i class="material-icons">clear</i>
              </button>
            </div>

          </div>
          <hr class="horizontal dark my-1"></hr>
          <div class="card-body pt-sm-3 pt-0">

            <div>
              <h6 class="mb-0">Sidebar Colors</h6>
            </div>
            <a href="javascript:void(0)" class="switch-trigger background-color">
              <div class="badge-colors my-2 text-start">
                <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
              </div>
            </a>

            <div class="mt-3">
              <h6 class="mb-0">Sidenav Type</h6>
              <p class="text-sm">Choose between 2 different sidenav types.</p>
            </div>
            <div class="d-flex">
              <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
            </div>
            <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

            <div class="mt-3 d-flex">
              <h6 class="mb-0">Navbar Fixed</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-3"></hr>
            <div class="mt-2 d-flex">
              <h6 class="mb-0">Light / Dark</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-sm-4"></hr>
            <a class="btn btn-outline-dark w-100" href="">View documentation</a>

            <div class="w-100 text-center">
              <span></span>
              <h6 class="mt-3">Thank you for sharing!</h6>
              <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
              </a>
              <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
              </a>

            </div>
          </div>
        </div>

      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
    </body>
  
               
                
                  
                
          <div></div>
         <div class="mrgn tttt">
                 <div class="col-12 my-4">
                 <div class="recherche my-4 mrgn">
            <input
              type="text"
              class="recherche"
              placeholder="Find par nom"
              ref={this.node}
              onClick={this.getVal}
              onChange={this.onChange}
            />&nbsp; &nbsp; &nbsp;
              <input
              type="text"
              class="recherche"
              placeholder="Find par etat"
              ref={this.node}
              onClick={this.getVal}
              onChange={this.onChangee}
            />
          </div>
                   <div class="card my-4">
                    
                     <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                       <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        
                         <h6 class="text-white text-capitalize ps-3">list des Candidats</h6>
                       </div>
                     </div>
                     <div >
                      
                     <div class="card-body px-0 pb-2">
                      
                       <div class="table-responsive p-0" >
                    
                         <table class="table align-items-center mb-0">
                           <thead>
                             <tr >
                               <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">fullName</th>
                               <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">date</th>
                               <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">tel</th>
                               <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">etatc</th>


                               <th class="text-secondary opacity-7">adresse</th>
                               <th class="text-secondary opacity-7">cv</th>
                               <th class="text-secondary opacity-7">commentaire</th>
                             </tr>
                           </thead>
                           <tbody>
                           {
                                                       this.state.Profile.map(
                                                           candidat =>
                                                          
                                                               <tr key = {candidat.candidatId}  onClick={ () =>  this.viewCandidat(candidat.candidatId)}>
                                                               
                                                                   <td >  <div class="d-flex px-2 py-1">
                                                                   <div class="d-flex flex-column justify-content-center">
                                                                   { candidat.fullName} </div>
                                                                    </div></td>
                                                                   <td > {candidat.date}</td>
                                                   
                                                                   <td > {candidat.tel}</td>
                                                                   <td> {candidat.etatc}</td>
                                                                   <td>{candidat.adresse}</td>
<td>                                                               
{ candidat.dBFile === null ? (
<Link onClick={ () => this.editCandidat(candidat.candidatId)}>affecter cv </Link> ): (<td>{candidat.dBFile.name}</td>)
}
</td>
<td>
{ candidat.commentaire === null ? (
<Link onClick={ () => this.affectComment(candidat.candidatId)}>affecter commentaire </Link> ): (<td>{candidat.commentaire.dernier_poste}</td>)
}</td>
<td>
{ candidat.commentaire === null ? (
<Link onClick={ () => this.affectComment(candidat.candidatId)}>affect commentaire </Link> ): (
<Link onClick={ () => this.exporterComment(candidat.commentaire.commentaireId)}>exporter commentaire </Link>)
}</td>



                                                                   <td>
                                                                 
                                                                   <button onClick={ () => this.updatecandidat(candidat.candidatId)} className="btn btn-info">modifier
                                                                   </button>
                                                                   <button onClick={ () => this.deletecandidat(candidat.candidatId)} className="btn btn-info">supprimer
                                                                   </button>                                                                  
                                                                   </td>
                                                               </tr>
                                                               
                                                       )
                                                   }


                           </tbody>
                           
                         </table>
                         </div>


                       </div>

                       <a href="/add" style={{marginLeft: "10px"}}  className="btn btn-info">ajouter Candidat </a>

                     </div>

                   </div>
                 </div>
               </div>
             
               </div>
              

        );
    }


}

export default ListCandidatComponent;
