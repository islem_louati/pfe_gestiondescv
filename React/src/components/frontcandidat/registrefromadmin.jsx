import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import authService from "../../services/auth.service";
import axios from "axios";
import { useParams } from "react-router-dom";


const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const email = value => {
    if (!isEmail(value)) {
        return (
            <div className="alert alert-danger" role="alert">
                This is not a valid email.
            </div>
        );
    }
};

const vusername = value => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class affectCandidatcreatebyasmintoUser extends Component {
   
    constructor(props) {
        super(props);
        console.log(this.props.match.params.id)
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeAppUserRole = this.onChangeAppUserRole.bind(this);

        this.state = {
            username: "",
            email: "",
            password: "",
            appUserRole:[],
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }
    onChangeAppUserRole(e) {
        this.setState({
            appUserRole: e.target.value
        });
    }

    handleRegister(e) {
      

        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();
       
        let user = { username:this.state.username,
            email: this.state.email,
            password:this.state.password,
            appUserRole :this.state.appUserRole}
        if (this.checkBtn.context._errors.length === 0) {
            axios.post(`http://localhost:8087/SpringMVC/api/v1/registration/registercandidat/${this.props.match.params.id}`, user)
             .then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                    this.props.history.push("/login");
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }
    render() {
        
        return (
    <main class="main-content mt-0 ps">
        <section>
          <div class="page-header min-vh-100">
            <div class="container">
              <div class="row">
                <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
                  <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center"
              >
                  </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
                  <div class="card card-plain">
                    <div class="card-header">
                      <h4 class="font-weight-bolder">Sign Up</h4>
                      <p class="mb-0">Enter your email and password to register</p>
                    </div>
                    <div class="card-body">
                      <form role="form">
                        <Form
                                              onSubmit={this.handleRegister}
                                              ref={c => {
                                                  this.form = c;
                                              }}
                                          >
                                            {!this.state.successful && (
                                            <div>

                         <div className="form-group">
                            <label htmlFor="username">Username</label>
                                <Input type="text" className="form-control" name="username" value={this.state.username} onChange={this.onChangeUsername}
                                    validations={[required,vusername]}/>
                         </div>
                         <div className="form-group">
                            <label htmlFor="username">Email</label>
                                <Input type="text" className="form-control" name="email" value={this.state.email} onChange={this.onChangeEmail}
                                    validations={[required,email]}/>
                         </div>
                         <div className="form-group">
                            <label htmlFor="password">password</label>
                                <Input type="password" className="form-control" name="password" value={this.state.password} onChange={this.onChangePassword}
                                    validations={[required]}/>
                         </div>
                         <div className="form-group" >
                                            <select onChange={(ddl)=>{this.setState({appUserRole:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--Role--</option>
                                                <option value="ADMIN">ADMIN</option>
                                                <option value="COMMERCIALE">COMMERCIALE</option>
                                                <option value="CANDIDAT">CANDIDAT</option>
                                            </select>
</div>

                        <div class="form-check form-check-info text-start ps-0">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked=""/>
                          <label class="form-check-label" for="flexCheckDefault">
                            I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and Conditions</a>
                          </label>
                        </div>
                        <div class="text-center">
                          <button type="button" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0" onClick={this.handleRegister}>Sign Up</button>

                        </div>
                        </div>
                        )}
                        {this.state.message && (
                                                    <div className="form-group">
                                                        <div
                                                            className={
                                                                this.state.successful
                                                                    ? "alert alert-success"
                                                                    : "alert alert-danger"
                                                            }
                                                            role="alert"
                                                        >
                                                            {this.state.message}
                                                        </div>
                                                    </div>
                                                )}
                                                <CheckButton
                                                    style={{ display: "none" }}
                                                    ref={c => {
                                                        this.checkBtn = c;
                                                    }}
                                                />
                         </Form>
                      </form>
                    </div>
                    <div class="card-footer text-center pt-0 px-lg-2 px-1">
                      <p class="mb-2 text-sm mx-auto">
                        Already have an account?
                        <a href="/login" class="text-primary text-gradient font-weight-bold">Sign in</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </main>

        );
    }
}
export default affectCandidatcreatebyasmintoUser;