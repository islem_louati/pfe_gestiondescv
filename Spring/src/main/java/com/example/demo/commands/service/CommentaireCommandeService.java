package com.example.demo.commands.service;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.Commentaire;
import com.example.demo.data.CommentaireRepository;
import com.example.demo.data.FileDB;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
//@RequiredArgsConstructor
@Component
public class CommentaireCommandeService {
	 private final Path root = Paths.get("uploads/files");
	private final CommentaireRepository cm;
	@Autowired
	private CandidatRepository repository;

	private final CandidatRepository rep;

	public CommentaireCommandeService(CommentaireRepository cm, CandidatRepository rep) {
		this.cm = cm;
		this.rep = rep;

	}

	public void createComment(Commentaire commentaire, int candidatId) {
		Candidat candidat = rep.findById(candidatId).orElse(null);

		commentaire.getCommentaireId();
		commentaire.getImpression_sourcing();
		commentaire.getDisposition_par_rapport_statutAE();
		commentaire.getNiveaux_francais();
		commentaire.getDisposition_negociation_salaire();
		commentaire.getDernier_poste();
		commentaire.getDate_relance();
		commentaire.getSalaire_negocie();

		this.cm.save(commentaire);
		candidat.setCommentaire(commentaire);
		repository.save(candidat);

	}

	public void updateComment(Commentaire commentaire, int commentaireId) {
		Commentaire com = cm.findById(commentaireId).orElse(null);
		Candidat c = com.getCandidat();
		System.out.println(c);
		if (com != null) {
			commentaire.setCommentaireId(commentaireId);
			commentaire.setCandidat(c);
			this.cm.save(commentaire);

		}
	}

	public void deleteComment(int commentaireId) {
		Commentaire com = cm.findById(commentaireId).orElse(null);

		if (com != null) {

			this.cm.deleteById(commentaireId);

		}
	}

	public void exportToPDF(HttpServletResponse response, int commentaireId) throws DocumentException, IOException {
		// Create the Object of Document
		Optional<Commentaire> cc = cm.findById(commentaireId);
		System.out.println(		cc.stream().collect(Collectors.toList()));
		cc.stream().collect(Collectors.toList());
		Document document = new Document(PageSize.A4);
	
		// get the document and write the response to output stream
		PdfWriter.getInstance(document, response.getOutputStream());
		document.open();
		// Add Font
		Font fontTiltle = FontFactory.getFont(FontFactory.TIMES_ROMAN);
		fontTiltle.setSize(20);
		fontTiltle.setColor(Color.GREEN);
		// Create Object of Paragraph
		Paragraph paragraph = new Paragraph("Comment List"+"\n", fontTiltle);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);
		// Add to the document
		document.add(paragraph);
		Paragraph d =new Paragraph("impression_sourcing"+" "+":");
		Paragraph diss =new Paragraph("disposition_par_rapport_statutAE"+" "+":");
		Paragraph francais =new Paragraph("niveaux_francais"+" "+":");
		Paragraph dis=new Paragraph("disposition_negociation_salaire"+" "+":");
		Paragraph p =new Paragraph("Dernier poste"+" "+":");
		Paragraph date =new Paragraph("date_relance"+" "+":");
		Paragraph salaire =new Paragraph("salaire_negocie"+" "+":");
		Paragraph candidat =new Paragraph("candidat"+" "+":");
		
		d.setAlignment(Paragraph.ALIGN_LEFT);
	
		diss.setAlignment(Paragraph.ALIGN_LEFT);
		dis.setAlignment(Paragraph.ALIGN_LEFT);
		p.setAlignment(Paragraph.ALIGN_LEFT);
		date.setAlignment(Paragraph.ALIGN_LEFT);
		salaire.setAlignment(Paragraph.ALIGN_LEFT);
		for (Commentaire c : cc.stream().collect(Collectors.toList())) {
		p.add(String.valueOf(c.getImpression_sourcing()));
		diss.add(String.valueOf(c.getDisposition_par_rapport_statutAE()));
		francais.add(String.valueOf(c.getNiveaux_francais()));
		dis.add(String.valueOf(c.getDisposition_negociation_salaire()));
		date.add(String.valueOf(c.getDate_relance()));
		salaire.add(String.valueOf(c.getSalaire_negocie()));
		candidat.add(String.valueOf(c.getCandidat()));

		d.add(c.getDernier_poste());
		// Add Font
		document.add(p);
		document.add(d);
		document.add(diss);
		document.add(dis);
		document.add(date);
		document.add(salaire);
		document.add(candidat);
		
		document.close();
		
		
	}
		}


	
}
