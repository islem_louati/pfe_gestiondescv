
import axios from 'axios';
import authHeader from './auth-header';
const  Client_API_BASE_URL = "http://localhost:8087/SpringMVC/client/clients";
const  ClientCreate_API_BASE_URL = "http://localhost:8087/SpringMVC/cliens/create";
const  Clientupdate_API_BASE_URL = "http://localhost:8087/SpringMVC/cliens/update";
const  Clientaffect_API_BASE_URL = "http://localhost:8087/SpringMVC/cliens/affect";
const  Clientdelete_API_BASE_URL = "http://localhost:8087/SpringMVC/cliens/delete";
const  ClientById_API_BASE_URL = "http://localhost:8087/SpringMVC/cliens/client";
const  ClientById_API_BASE_URL_sans = "http://localhost:8087/SpringMVC/client/clientsanscome";
const  ClientById_API_BASE_URL_avec = "http://localhost:8087/SpringMVC/client/clientsaveccome"

class ClientService {
    getclients(){
        return axios.get(Client_API_BASE_URL,{ headers: authHeader() });
    }
    getclientssans(){
        return axios.get(ClientById_API_BASE_URL_sans,{ headers: authHeader() });
    }
    getclientsavec(){
        return axios.get(ClientById_API_BASE_URL_avec,{ headers: authHeader() });
    }
    createClient(client){
        return axios.post(ClientCreate_API_BASE_URL, client,{ headers: authHeader() });
    }
    updateClient(client,clientId){
        return axios.post(Clientupdate_API_BASE_URL+'/'+clientId, client,{ headers: authHeader() });
    }

    ClientbyId(clientId){
        return axios.get(ClientById_API_BASE_URL+'/'+clientId,{ headers: authHeader() });
    }
    affectcommercialeToClient(client){
        return axios.post(ClientCreate_API_BASE_URL, client,{ headers: authHeader() });
    }

   
    

}

export default new ClientService();