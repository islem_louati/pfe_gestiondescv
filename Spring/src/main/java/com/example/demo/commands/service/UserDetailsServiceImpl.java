package com.example.demo.commands.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  AppUserRepository userRepository;
  @Transactional
  @Override
  public UserDetails loadUserByUsername(String username)
          throws UsernameNotFoundException {
    
  
      AppUser user = userRepository.getByUsername(username)
      		.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

      return UserDetailsImpl.build(user);
  }

}
