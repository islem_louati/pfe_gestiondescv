import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
import "bootstrap/dist/css/bootstrap.min.css";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { TextField } from "@material-ui/core";

export default class Profilee extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" }
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();

    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    const { currentUser } = this.state;
    const filterOptions = createFilterOptions({
      matchFrom: 'start',
      stringify: option => option,
    });
    
    // Sample options for search box
    var myOptions = AuthService.getCurrentUser();
    return (

      <div>
              { currentUser.appUserRole === 'ADMIN' ? 
      <body>
     <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href=" https://demos.creative-tim.com/material-dashboard/pages/dashboard " target="_blank">
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>

      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>



        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">login</i>
            </div>
            <span class="nav-link-text ms-1" >Profile</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/registre">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">assignment</i>
            </div>
            <span class="nav-link-text ms-1">Comment</span>
          </a>
        </li>
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
  </aside>
      
      <div class="fixed-plugin ps">
     
      <div class="card shadow-lg">
      <div class="card-header pb-0 pt-3">
      <div class="float-start">
        <h5 class="mt-3 mb-0">Material UI Configurator</h5>
        <p>See our dashboard options.</p>
      </div>
      <div class="float-end mt-4">
        <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
          <i class="material-icons">clear</i>
        </button>
      </div>
      
      </div>
      <hr class="horizontal dark my-1"></hr>
      <div class="card-body pt-sm-3 pt-0">
      
      <div>
        <h6 class="mb-0">Sidebar Colors</h6>
      </div>
      <a href="javascript:void(0)" class="switch-trigger background-color">
        <div class="badge-colors my-2 text-start">
          <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
          <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
          <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
          <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
          <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
          <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
        </div>
      </a>
      
      <div class="mt-3">
        <h6 class="mb-0">Sidenav Type</h6>
        <p class="text-sm">Choose between 2 different sidenav types.</p>
      </div>
      <div class="d-flex">
        <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
        <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
        <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
      </div>
      <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
      
      <div class="mt-3 d-flex">
        <h6 class="mb-0">Navbar Fixed</h6>
        <div class="form-check form-switch ps-0 ms-auto my-auto">
          <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
        </div>
      </div>
      <hr class="horizontal dark my-3"></hr>
      <div class="mt-2 d-flex">
        <h6 class="mb-0">Light / Dark</h6>
        <div class="form-check form-switch ps-0 ms-auto my-auto">
          <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
        </div>
      </div>
      <hr class="horizontal dark my-sm-4"></hr>
      <a class="btn btn-outline-dark w-100" href="">View documentation</a>
      
      <div class="w-100 text-center">
        <span></span>
        <h6 class="mt-3">Thank you for sharing!</h6>
        <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
          <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
        </a>
        <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
          <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
        </a>
      
      </div>
      </div>
      </div>
      
      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
      </body>:  currentUser.appUserRole === 'COMMERCIALE' ?    
        <body>
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
<div class="sidenav-header">
<i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>

<img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>


</div>
<hr class="horizontal light mt-0 mb-2"></hr>
<div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
<ul class="navbar-nav">
<li class="nav-item">
<a class="nav-link text-white active BgOfLink" href="/list">
<div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
  <i class="material-icons opacity-10">dashboard</i>
</div>
<span class="nav-link-text ms-1">Quiz</span>
</a>
</li>
<li class="nav-item mt-3">
<h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
</li>
<li class="nav-item">
<a class="nav-link text-white " href="/candidat">
<div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
  <i class="material-icons opacity-10">person</i>
</div>
<span class="nav-link-text ms-1">Candidat</span>
</a>
</li>
<li class="nav-item">
<a class="nav-link text-white " href="/client">
<div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
  <i class="material-icons opacity-10">person</i>
</div>
<span class="nav-link-text ms-1">Client</span>
</a>
</li>
<li class="nav-item">
      <a class="nav-link text-white " href="/missions">
        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
          <i class="material-icons opacity-10">person</i>
        </div>
        <span class="nav-link-text ms-1">Mission</span>
      </a>
</li>



<li class="nav-item">
<a class="nav-link text-white " href="/login">
<div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
  <i class="material-icons opacity-10">login</i>
</div>
<span class="nav-link-text ms-1" >Profile</span>
</a>
</li>

</ul>
<div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
<div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
</div>
</div>
<div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
<div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
</div>
</div>
</div>
<div class="sidenav-footer position-absolute w-100 bottom-0 ">
<div class="mx-3">
<a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
</div>
</div>
<div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
<div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
</div>
<div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
<div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
</aside>

<div class="fixed-plugin ps">

<div class="card shadow-lg">
<div class="card-header pb-0 pt-3">
<div class="float-start">
  <h5 class="mt-3 mb-0">Material UI Configurator</h5>
  <p>See our dashboard options.</p>
</div>
<div class="float-end mt-4">
  <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
    <i class="material-icons">clear</i>
  </button>
</div>

</div>
<hr class="horizontal dark my-1"></hr>
<div class="card-body pt-sm-3 pt-0">

<div>
  <h6 class="mb-0">Sidebar Colors</h6>
</div>
<a href="javascript:void(0)" class="switch-trigger background-color">
  <div class="badge-colors my-2 text-start">
    <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
    <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
    <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
    <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
    <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
    <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
  </div>
</a>

<div class="mt-3">
  <h6 class="mb-0">Sidenav Type</h6>
  <p class="text-sm">Choose between 2 different sidenav types.</p>
</div>
<div class="d-flex">
  <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
  <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
  <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
</div>
<p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

<div class="mt-3 d-flex">
  <h6 class="mb-0">Navbar Fixed</h6>
  <div class="form-check form-switch ps-0 ms-auto my-auto">
    <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
  </div>
</div>
<hr class="horizontal dark my-3"></hr>
<div class="mt-2 d-flex">
  <h6 class="mb-0">Light / Dark</h6>
  <div class="form-check form-switch ps-0 ms-auto my-auto">
    <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
  </div>
</div>
<hr class="horizontal dark my-sm-4"></hr>
<a class="btn btn-outline-dark w-100" href="">View documentation</a>

<div class="w-100 text-center">
  <span></span>
  <h6 class="mt-3">Thank you for sharing!</h6>
  <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
    <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
  </a>
  <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
    <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
  </a>

</div>
</div>
</div>

<div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
<div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
</div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
<div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
</body>:      <div>
                <br/>
              <br/>
              <br/>
<div >
  <div id="status">&nbsp;</div>
</div>

<a href="javascript:" id="return-to-top"><span class="fa fa-arrow-up" aria-hidden="true"></span></a>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light navbar-fixed-top" id="ftco-navbar">
	    <div class="container-fluid">
	    
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="fa fa-bars"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav m-auto">
	        	<li class="nav-item active"><a href="/profilee" class="nav-link">profile</a></li>
	        	<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Page</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
              	<a class="dropdown-item" href="#">Page 1</a>
                <a class="dropdown-item" href="#">Page 2</a>
                <a class="dropdown-item" href="#">Page 3</a>
                <a class="dropdown-item" href="#">Page 4</a>
              </div>
            </li>
	        	<li class="nav-item"><a href="#" class="nav-link">Work</a></li>
	        	<li class="nav-item"><a href="#" class="nav-link">Blog</a></li>
	          <li class="nav-item"><a href="#" class="nav-link">Contact</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>

<header class="intro">
  <div class="intro-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="brand-heading">
            <span class="txt-rotate" data-period="100" data-rotate='[ "Hello,", "Olá,", "Hallo,", "Salut,", "Ciao,", "Hola,", "Hej," ]'></span>
          </div>
       
          <p class="intro-text">DigitalCook
            <br/><span class="fa fa-code" aria-hidden="true"></span> <span class="intro-text-hgl"> Prestataire de services informatiques
pour votre entreprise</span> <span class="fa fa-code" aria-hidden="true"></span>
            <br/>installé <span class="city">Paris</span>, France.</p>
          <div class="mouse-warp">
            <a href="#mouse-here" class="page-scroll">
              <div class="mouse">
                <div class="scroll"></div>
              </div>
            </a>
         
       
        </div>
        <span class="fa fa-user" aria-hidden="true" style={{color: '#f5487f'}}></span>
       
       <p  style={{fontsize: '20px',color: '#ffffff',lineheight:'30px',textalign: 'center'}}>
      Vous êtes un candidat à la recherche d'un nouveau challenge professionnel ?</p>
          
      
        </div>
      </div>
    </div>
  </div>
</header>
<div id="mouse-here"></div>




<footer id="contact">
  <div class="container">
    <div class="row">
      <h2>CONTACT</h2>
      <p>You can contact me at: 9.marques.marina[AT]gmail.com</p>
      <br/>
      <div class="social">
        <a href="https://codepen.io/shvvffle/" target="_blank" alt="CodePen profile"><span class="fa fa-codepen" aria-hidden="true"></span></a>
        <a href="https://pt.linkedin.com/in/marinamarques9" target="_blank" alt="LinkedIn profile"><span class="fa fa-linkedin" aria-hidden="true"/></a>
        <a href="https://github.com/shvvffle" target="_blank" alt="GitHub profile"><span class="fa fa-github" aria-hidden="true"></span></a>
      </div>
      <p id="copyright"></p>
    </div>

  </div>

</footer>

            </div>


}


      <div className="container one ">
        {(this.state.userReady) ?
        <div>
        <header className="jumbotron">
          <h3>
            <strong>{currentUser.username}</strong> Profile
          </h3>
        </header>
        <p>
          <strong>Email:</strong>{" "}
          {currentUser.email}
        </p>
        <strong>Authorities:</strong>
        <ul>
        { currentUser.appUserRole === null && currentUser.appUserRole === 'COMMERCIALE' ? 
<a href ={`addcommerciale/${currentUser.id}`}>completer profile </a> : <td>{currentUser.appUserRole}</td>
}
         
        </ul>
      </div>: null}
      </div>
      </div>
    );
  }
}
