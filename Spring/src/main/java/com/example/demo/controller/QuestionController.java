package com.example.demo.controller;

import io.swagger.annotations.ApiOperation;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.commands.QuestionService;
import com.example.demo.data.QuestionEntity;
import com.example.demo.data.dto.AnswerDto;
import com.example.demo.data.dto.QuestionDto;
import com.example.demo.model.AnswerRequestModel;
import com.example.demo.model.QuestionRequestModel;
import com.example.demo.model.response.QuestionResponseModel;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("questions") // http:localhost:8080/questions
public class QuestionController {

    @Autowired
    QuestionService questionService;

    private ModelMapper modelMapper = new ModelMapper();

    @CrossOrigin
    @PostMapping
    @RequestMapping("create")
    public void createQuestion(@RequestBody QuestionEntity question){
        //QuestionDto questionDto = modelMapper.map(question, QuestionDto.class);

       questionService.createQuestion(question);

    }

    @CrossOrigin
    @GetMapping(path = "/{id}")
    @ApiOperation(value = "getQuestionById")
    public QuestionResponseModel getQuestionById(@PathVariable long id){
        return modelMapper.map(questionService.getQuestionById(id), QuestionResponseModel.class);
    }

    @CrossOrigin
    @GetMapping
    @RequestMapping("q")
    @ApiOperation(value = "getAllQuestions")
    public Set<QuestionResponseModel> getAllQuestions(){
        Set<QuestionResponseModel> returnValue = new HashSet<>();
        for(QuestionDto questionDto: questionService.getAllQuestions()){
            QuestionResponseModel questionResponseModel= modelMapper.map(questionDto, QuestionResponseModel.class);
            returnValue.add(questionResponseModel);
        }

        return returnValue;
    }

    @CrossOrigin
    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "deleteQuestionById")
    public String deleteQuestionById(@PathVariable long id){

        questionService.deleteQuestionById(id);
        return "Question was successfully deleted";

    }

    @CrossOrigin
    @PutMapping(path = "update/{id}")
    @ApiOperation(value = "addAnswerToQuestion")
    public QuestionResponseModel addAnswerToQuestion(@PathVariable long id, @RequestBody AnswerRequestModel answer){

        AnswerDto answerToAdd = modelMapper.map(answer, AnswerDto.class);

        return modelMapper.map(questionService.updateAnswer(answerToAdd, id), QuestionResponseModel.class);

    }

//    @CrossOrigin
//    @PutMapping(path = "/{id}")
//    @ApiOperation(value = "updateQuestion")
//    public QuestionResponseModel updateQuestion(@PathVariable long id, @RequestBody QuestionRequestModel question){
//
//        QuestionDto questionToAdd = modelMapper.map(question, QuestionDto.class);
//
//        return modelMapper.map(questionService.updateQuestion(questionToAdd, id), QuestionResponseModel.class);
//    }


}
