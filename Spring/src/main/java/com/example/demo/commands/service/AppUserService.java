package com.example.demo.commands.service;

import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.AppUserRole;
import com.example.demo.email.EmailSender;
import com.example.demo.registration.token.ConfirmationToken;
import com.example.demo.registration.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.security.crypto.password.PasswordEncoder;
@Service
@Transactional
public class AppUserService {

    private final static String USER_NOT_FOUND_MSG =
            "user with email %s not found";

    private final AppUserRepository appUserRepository;
    @Autowired
    PasswordEncoder encoder;
    private final ConfirmationTokenService confirmationTokenService;
	public AppUserService(AppUserRepository appUserRepository,ConfirmationTokenService confirmationTokenService) {
		this.appUserRepository = appUserRepository;
		
		this.confirmationTokenService=confirmationTokenService;
	
	}

//    @Override
//    public UserDetails loadUserByUsername(String username)
//            throws UsernameNotFoundException {
//        return appUserRepository.findByUsername(username)
//                .orElseThrow(() ->
//                        new UsernameNotFoundException(
//                                String.format(USER_NOT_FOUND_MSG, username)));
//    }
    public String findUsernameById(int id) {
        return appUserRepository.getUsernameByUserId(id);
    }
    public void findById(Long userId) {
         appUserRepository.findById(userId);
    }
    public String signUpUser(AppUser appUser) {
        boolean userExists = appUserRepository
                .getByEmail(appUser.getEmail())
                .isPresent();
        boolean userExistss = appUserRepository
                .getByUsername(appUser.getUsername())
                .isPresent();
String message="";


        if (userExists) {
            // TODO check of attributes are the same and
            // TODO if email not confirmed send confirmation email.
       	 message = "email exist " + appUser.getEmail();
             throw new IllegalStateException(message);     
        }
             else if (userExistss) {
            // TODO check of attributes are the same and
            // TODO if email not confirmed send confirmation email.
       	 message = "username exist " + appUser.getUsername();
            throw new IllegalStateException(message);     
        }

       

        String encodedPassword = encoder
                .encode(appUser.getPassword());

        appUser.setPassword(encodedPassword);
 
        appUser.getAppUserRole();
       
        appUserRepository.save(appUser);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );

        confirmationTokenService.saveConfirmationToken(
                confirmationToken);

//        TODO: SEND EMAIL

        return token;
    }

    public int enableAppUser(String email) {
        return appUserRepository.enableAppUser(email);
        
    }

	
}
