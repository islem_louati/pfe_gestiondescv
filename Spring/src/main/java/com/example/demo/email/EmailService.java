package com.example.demo.email;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;



import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service

public class EmailService implements EmailSender{

    private final static Logger LOGGER = LoggerFactory
            .getLogger(EmailService.class);

    private final JavaMailSender mailSender;
	public EmailService(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

		
    @Async
    public void sendUser(String too, String emaill) {
    	
        try {
            MimeMessage mime = mailSender.createMimeMessage();
            MimeMessageHelper helper =
           new MimeMessageHelper(mime, "utf-8");
            helper.setText(emaill, true);
            helper.setTo(too);
            helper.setSubject("Confirm your email");
            helper.setFrom("islemlouati1920@gmail.com");
            mailSender.send(mime);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }
    
    @Async
    public void sendTocommerciale(String tocom, String emailcom) {
    	
        try {
            MimeMessage mime = mailSender.createMimeMessage();
            MimeMessageHelper helper =
           new MimeMessageHelper(mime, "utf-8");
            helper.setText(emailcom, true);
            helper.setTo(tocom);
            helper.setSubject("Confirm your email");
            helper.setFrom("islemlouati1920@gmail.com");
            mailSender.send(mime);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }
		
    @Async
    public void send(String to, String email) {
    	
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper =
           new MimeMessageHelper(mimeMessage, "utf-8");
            helper.setText(email, true);
            helper.setTo("islemlouati1920@gmail.com");
            helper.setSubject("Confirm your email");
            helper.setFrom("islem.louati.fr@gmail.com");
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }
    @Async
    public void sendfront(String tooo, String emailll) {
    	
        try {
            MimeMessage mime = mailSender.createMimeMessage();
            MimeMessageHelper helper =
           new MimeMessageHelper(mime, "utf-8");
            helper.setText(emailll, true);
            helper.setTo("islem.louati@esprit.tn");
            helper.setSubject("cette candidat a passer le test technique passable");
            helper.setFrom("islemlouati1920@gmail.com");
            mailSender.send(mime);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }
}
