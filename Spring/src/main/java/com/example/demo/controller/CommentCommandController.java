package com.example.demo.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commands.service.CommentaireCommandeService;
import com.example.demo.commands.service.MissionCommandService;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.Commentaire;
import com.example.demo.data.CommentaireRepository;
import com.example.demo.data.Mission;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/Comment")
public class CommentCommandController {
	private final CandidatRepository rep;
	private final CommentaireCommandeService commentCommandService ;
	@Autowired
	private  CommentaireRepository cm;
	public CommentCommandController(CommentaireCommandeService commentCommandService, CandidatRepository rep) {
		this.rep = rep;
		this.commentCommandService = commentCommandService;
	}
	@GetMapping("/comm/{commentaireId}")
	public void exportToPDF (HttpServletResponse response,@PathVariable int commentaireId) throws IOException
	{
		response.setContentType("application/pdf");
		  DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");
		  String currentDateTime = dateFormat.format(new Date());
		  String headerkey = "Content-Disposition";
		  String headervalue = "attachment; filename=pdf_"+currentDateTime+".pdf";
		  response.setHeader(headerkey, headervalue);
			Optional<Commentaire> cc = cm.findById(commentaireId);
			
		commentCommandService.exportToPDF(response, commentaireId);
	}
	@PostMapping("/create/{candidatId}")
	public ResponseEntity<String> createComment(@RequestBody Commentaire commentaire , @PathVariable int candidatId) {

		try {
			commentCommandService.createComment(commentaire,candidatId);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
	
	@PutMapping("/update/{commentaireId}")
	public ResponseEntity<String> updateComment(@RequestBody Commentaire commentaire , @PathVariable int commentaireId) {

		try {
			 commentCommandService.updateComment(commentaire,commentaireId);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
	@DeleteMapping("/delete/{commentaireId}")
	public ResponseEntity<String> deleteComment(@PathVariable int commentaireId) {

		try {
			 commentCommandService.deleteComment(commentaireId);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
}
