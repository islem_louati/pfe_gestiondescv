import { Component } from "react";
import './home.css'

export default class homeClientt extends Component{
    render(){
        return(
            <div>
                <br/>
              <br/>
              <br/>
<div >
  <div id="status">&nbsp;</div>
</div>

<a href="javascript:" id="return-to-top"><span class="fa fa-arrow-up" aria-hidden="true"></span></a>



<header class="intro">
  <div class="intro-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="brand-heading">
            <span class="txt-rotate" data-period="100" data-rotate='[ "Hello,", "Olá,", "Hallo,", "Salut,", "Ciao,", "Hola,", "Hej," ]'></span>
          </div>
       
          <p class="intro-text">DigitalCook
            <br/><span class="fa fa-code" aria-hidden="true"></span> <span class="intro-text-hgl"> Prestataire de services informatiques
pour votre entreprise</span> <span class="fa fa-code" aria-hidden="true"></span>
            <br/>installé <span class="city">Paris</span>, France.</p>
          <div class="mouse-warp">
            <a href="#mouse-here" class="page-scroll">
              <div class="mouse">
                <div class="scroll"></div>
              </div>
            </a>
         
       
        </div>
        <span class="fa fa-user" aria-hidden="true" style={{color: '#f5487f'}}></span>
       
       <p  style={{fontsize: '20px',color: '#ffffff',lineheight:'30px',textalign: 'center'}}>
      Vous êtes un candidat à la recherche d'un nouveau challenge professionnel ?</p>
          
      
        </div>
      </div>
    </div>
  </div>
</header>
<div id="mouse-here"></div>


<section id="about-me">
  <div class="container">
    <div class="row">
      <h2>ABOUT</h2>
      <div class="col-lg-6 about-intro">
        <div class="about-inner">
          <div class="about-ct">
            <img src="http://marinamarques.pt/img/mm_info.jpg" alt="Marina Marques" class="img-responsive"/>
            <div class="textbox-about hidden-xs">
              <div class="btn button-social">
                <a href="https://codepen.io/shvvffle/" target="_blank" alt="CodePen profile"><span class="fa fa-codepen" aria-hidden="true"></span></a>
                <a href="https://pt.linkedin.com/in/marinamarques9" alt="LinkedIn profile" target="_blank"><span class="fa fa-linkedin" aria-hidden="true"></span></a>
                <a href="https://github.com/shvvffle" target="_blank" alt="GitHub profile"><span class="fa fa-github" aria-hidden="true"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 about-intro-text">
        <p>Digitalcook, un atout pour votre entreprise <span>Expert dans la prestation des services informatiques</span> <br/> Engagés dans une démarche qualité, 
        toutes nos promesses sont tenues, et les résultats garantis!.<br/> Dotés d’un savoir-faire dans le<span> domaine informatique,</span> DigitalCook tient à répondre à chacun de vos besoins et à 
        comprendre vos enjeux. En tant que <span>prestataire de services numériques, </span> 
        nous vous recommandons des solutions sur mesure afin de garantir la performance de votre système d’information.
        <br/>
        Nous vous proposons entre autres le monitoring de l’activité de vos serveurs,
        <span>la sécurisation de votre réseau informatique </span>  et la <span> protection de vos données.</span> Ainsi que des visites régulières sur votre site pour effectuer les travaux de maintenance. DigitalCook vous accompagne et vous conseille. Vous pourrez dès lors vous développer rapidement et durablement.
         <span class="fa fa-beer" aria-hidden="true"></span></p>
     
      </div>
    </div>
  
  </div>



</section>



<section id="portfolio">
  <div class="container-fluid portfolio-ct">
    <h2>Nos services Informatique</h2>
    <div class="row">
      <div class="col-sm-6 col-md-4 grid-portfolio">
        <div class="portfolio-item-ct">
          <div class="thumbnail">
            <img src="http://marinamarques.pt/img/work-personalWebsite.png" alt="Portfolio - IBV MEXICO" class="img-responsive"/>
            <div class="textbox-portfolio">
              <div class="button-weblink">
                <a href="https://ibv-mx.bnpparibas.com/" alt="Portfolio - IBV MEXICO" target="_blank"><span class="fa fa-link portfolio-link" aria-hidden="true"></span></a>
              </div>
            </div>
          </div>
          <div class="caption portfolio-details">
            <h3>Installation matériel</h3>
            <p>Installation, câblage, gestion de parc,
              <br/>  configuration, maintenance et déploiement du matériel informatique
            </p>
            <ul class="portfolio-tags">
              <li class="portfolio-tag">WORDPRESS</li>
              <li class="portfolio-tag">CHILD THEME</li>
              <li class="portfolio-tag">SEO</li>
            </ul>
          </div>
        </div>
      </div>
  
      <div class="col-sm-6 col-md-4 grid-portfolio">
        <div class="portfolio-item-ct">
          <div class="thumbnail">
            <img src="http://marinamarques.pt/img/work-areyoufeelinglow.png" alt="Portfolio - ARE YOU FEELING LOW?" class="img-responsive"/>
            <div class="textbox-portfolio">
              <div class="button-weblink">
                <a href="https://shvvffle.github.io/areyoufeelinglow/" target="_blank" alt="Portfolio - ARE YOU FEELING LOW?"><span class="fa fa-link portfolio-link" aria-hidden="true"></span></a>
              </div>
            </div>
          </div>
          <div class="caption portfolio-details">
            <h3>Infrastructure technique</h3>
            <p>Mise en œuvre des tests unitaires et globaux, 
              <br/>intégration, déploiement, adaptation et assistance de premier niveau
            </p>
            <ul class="portfolio-tags">
              <li class="portfolio-tag">HTML5 / CSS3</li>
              <li class="portfolio-tag">BOOTSTRAP</li>
              <li class="portfolio-tag">JAVASCRIPT</li>
              <li class="portfolio-tag">SEO</li>
            </ul>
          </div>
        </div>
      </div>
  
      <div class="col-sm-6 col-md-4 grid-portfolio">
        <div class="portfolio-item-ct">
          <div class="thumbnail">
            <img src="http://marinamarques.pt/img/work-personalWebsite.png" alt="Portfolio - PERSONAL WEBSITE" class="img-responsive"/>
            <div class="textbox-portfolio">
              <div class="button-weblink">
                <a href="#" alt="Portfolio - PERSONAL WEBSITE"><span class="fa fa-link portfolio-link" aria-hidden="true"></span></a>
              </div>
            </div>
          </div>
          <div class="caption portfolio-details">
            <h3>Intégration applicative</h3>
            <p>Mise en œuvre, administration, 
              <br/> étude de vulnérabilités et migration des infrastructures informatiques
            </p>
            <ul class="portfolio-tags">
              <li class="portfolio-tag">HTML5 / CSS3</li>
              <li class="portfolio-tag">BOOTSTRAP</li>
              <li class="portfolio-tag">JAVASCRIPT</li>
              <li class="portfolio-tag">SEO</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<section id="testimionals">
  <div class="testimionals-bg">
    <div class="container testimionals-bg-opacity">
      <h2>TESTIMONIALS</h2>
      <div><span class="fa fa-quote-left quotes" aria-hidden="true"></span>
      </div>
      <div id="slider-testimionals" class="carousel slide" data-ride="carousel">
   
        <ol class="carousel-indicators">
          <li data-target="#slider-testimionals" data-slide-to="0" class="active"></li>
          <li data-target="#slider-testimionals" data-slide-to="1"></li>
          <li data-target="#slider-testimionals" data-slide-to="2"></li>
          <li data-target="#slider-testimionals" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <div class="carousel-caption">
                  <p>Marina is always ready to accomplish the impossible in record time.</p>
                  <p class="client">Emilien, Marketing department - BNP Paribas</p>
                </div>
              </div>
            </div>
          </div>
         
          <div class="item">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <div class="carousel-caption">
                  <p>She's the fastest person in BNPP! Quick as lightning with robust solutions.</p>
                  <p class="client">Rogier, Sales department - BNP Paribas</p>
                </div>
              </div>
            </div>
          </div>
      
          <div class="item">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <div class="carousel-caption">
                  <p>She shows real commitment and demonstrates entrepreneurship skills as well as innovation abilities.</p>
                  <p class="client">Gillian, Manager - BNP Paribas</p>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <div class="carousel-caption">
                  <p>She doesn't give up until she finds a solution for what seems to be impossible.</p>
                  <p class="client">Ana, Manager - Páginas Amarelas</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<footer id="contact">
  <div class="container">
    <div class="row">
      <h2>CONTACT</h2>
      <p>You can contact me at: 9.marques.marina[AT]gmail.com</p>
      <br/>
      <div class="social">
        <a href="https://codepen.io/shvvffle/" target="_blank" alt="CodePen profile"><span class="fa fa-codepen" aria-hidden="true"></span></a>
        <a href="https://pt.linkedin.com/in/marinamarques9" target="_blank" alt="LinkedIn profile"><span class="fa fa-linkedin" aria-hidden="true"/></a>
        <a href="https://github.com/shvvffle" target="_blank" alt="GitHub profile"><span class="fa fa-github" aria-hidden="true"></span></a>
      </div>
      <p id="copyright"></p>
    </div>

  </div>

</footer>

            </div>
        );
    }
}