import axios from 'axios';
import authHeader from './auth-header';




const  COMMERCIALE_API_BASE_URL = "http://localhost:8087/SpringMVC/commerciale/usercomeciale";
const  COMMERCIALECREATE_API_BASE_URL = "http://localhost:8087/SpringMVC/commerciale/create";
const  COMMERCIALE_BYID_API_BASE_URL = "http://localhost:8087/SpringMVC/users/getcommerciale";
const  CANDIDAT_DELETE_API_BASE_URL = "http://localhost:8087/SpringMVC/users/usercommerciale";
const  CANDIDAT_API_BASE_URL = "http://localhost:8087/SpringMVC/users/commerciale";

const  CANDIDAT_Commerciale_API_BASE_URL = "http://localhost:8087/SpringMVC/api/v1/registration/users";
const CANDIDAT_nonenable_API_BASE_URL="http://localhost:8087/SpringMVC/users/usercommerciale"
;
    class CommercialeService{
        getcommerciale(){
            return axios.get(CANDIDAT_API_BASE_URL ,{ headers: authHeader() });
        }

        getusercommerciale(){
            return axios.get(CANDIDAT_Commerciale_API_BASE_URL ,{ headers: authHeader() });
        }
        createCommerciale(commerciale){
        
            return axios.post(COMMERCIALECREATE_API_BASE_URL, commerciale, { headers: authHeader() });

        }
        getCommercialeById(userid) {
            console.log(userid)
            return axios.get(COMMERCIALE_BYID_API_BASE_URL+'/'+userid,{ headers: authHeader() });
           
        }
        
        getusernonenable(){
            return axios.get(CANDIDAT_nonenable_API_BASE_URL ,{ headers: authHeader() });
        }



}
export default new CommercialeService()