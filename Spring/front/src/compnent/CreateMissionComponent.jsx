import React, {Component} from 'react';
import Mission from "../Service/Mission";

class CreateMissionComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            description: '',
            datedebut: '',
            datefin: '',
            etat: ''
        }
    }
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            Mission.getmissions(this.state.id).then( (res) =>{
                let mission = res.data;
                this.setState({description: mission.description,
                    datedebut: mission.datedebut,
                    datefin: mission.datefin,
                    etat: mission.etat,

                });
            });
        }
    }

    saveOrUpdateMission = (m) => {
        m.preventDefault();
        let missions = {description: this.state.description, datedebut: this.state.datedebut,datefin:this.state.datefin,etat: this.state.etat(

            )};
        console.log('mission => ' + JSON.stringify(missions)
            .bind(this));

        // step 5

        Mission.createMission(missions)
        .then(res =>{
            this.props.history.push('/missions');
        })
        .catch((error) => {
                // Error
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    // console.log(error.response.data);
                    // console.log(error.response.status);
                    // console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the
                    // browser and an instance of
                    // http.ClientRequest in node.js
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });



    }
    changeDescriptionHandler= (event) => {
        this.setState({description: event.target.value});
    }

    changedatedebutHandler= (event) => {
        this.setState({datedebut: event.target.value});
    }



    changedatefinHandler= (event) => {
        this.setState({datefin: event.target.value});
    }
    changeetatHandler= (event) => {
        this.setState({etat: event.target.value});
    }


    cancel(){
        this.props.history.push('/missions');
    }

    getTitle() {
        if (this.state.id === '_add') {
            return <h3 className="text-center">Add mission</h3>
        } else {
            return <h3 className="text-center">Update Mission</h3>
        }
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "container">
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> Description: </label>
                                        <input placeholder="description" name="description" className="form-control"
                                               value={this.state.description} onChange={this.changeDescriptionHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> datedebut: </label>
                                        <input placeholder="datedebut " name="datedebut" type="date" className="form-control"
                                               value={this.state.datedebut} onChange={this.changedatedebutHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> datefin: </label>
                                        <input placeholder="datefin" name="datefin" type="date" className="form-control"
                                               value={this.state.datefin} onChange={this.changedatefinHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> etat: </label>
                                        <input placeholder="etat" name="etat" className="form-control"
                                               value={this.state.etat} onChange={this.changeetatHandler}/>
                                    </div>



                                    <button className="btn btn-success" onClick={this.saveOrUpdateMission}>Save</button>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default CreateMissionComponent;