//package com.example.demo.controller;
//
//import java.io.IOException;
//import java.util.Date;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.example.demo.commands.service.RoleService;
//import com.example.demo.data.Candidat;
//import com.example.demo.data.Droits_Speciaux;
//import com.example.demo.data.Entite_commerciale;
//import com.example.demo.data.Expertise;
//import com.example.demo.data.Role;
//import com.example.demo.data.Role_dorigine;
//import com.example.demo.data.Titre;
//import com.example.demo.data.UserRepository;
//
//@RestController
//@RequestMapping("/role")
//public class RoleController {
//	@Autowired
//	private final RoleService user;
//
//	public RoleController(RoleService user) {
//		this.user = user;
//	}
//	@PostMapping("/create")
//	@PreAuthorize("hasRole('ADMIN')")
//	public ResponseEntity<String> createAccount(@RequestBody Role request) {
//		try {
//			Role response = user.createRole(request);
//
//			return new ResponseEntity<>( HttpStatus.CREATED);
//		} catch (Exception e) {
//			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//
//	}
//	
//}
