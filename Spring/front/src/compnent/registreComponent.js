import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import AuthService from "../Service/auth.service";


const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const email = value => {
    if (!isEmail(value)) {
        return (
            <div className="alert alert-danger" role="alert">
                This is not a valid email.
            </div>
        );
    }
};

const vusername = value => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class Register extends Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeAppUserRole = this.onChangeAppUserRole.bind(this);

        this.state = {
            username: "",
            email: "",
            password: "",
            appUserRole:"",
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }
    onChangeAppUserRole(e) {
        this.setState({
            appUserRole: e.target.value
        });
    }

    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.register(
                this.state.username,
                this.state.email,
                this.state.password,
                this.state.appUserRole
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                    window.alert("verify your account");
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }
    render() {
        return (
          <div class="container">
                     <div class="row">
                         <div class="col-md-6">


                    <Form
                        onSubmit={this.handleRegister}
                        ref={c => {
                            this.form = c;
                        }}
                    >
                        {!this.state.successful && (

                                 <div class="card">
                                <div  class="box">
                                 <h1>registre</h1>
                               <p class="text-muted"> Please enter your login and password!</p>

                                    <Input
                                            type="text" name=""  placeholder="Create your Username"

                                        value={this.state.username}onChange={this.onChangeUsername}
                                        validations={[required, vusername]}
                                    />




                                    <Input
                                    type="text" name=""  placeholder="Enter your email address"
                                        value={this.state.email}onChange={this.onChangeEmail}validations={[required, email]}
                                    />



                                    <Input
                                    type="password" name="" placeholder="Username"
                                         placeholder="Create your password"

                                        value={this.state.password}onChange={this.onChangePassword}

                                    />




                                    <Input
                                   type="text" name="" placeholder="role"
                                        name="appUserRole"
                                        value={this.state.appUserRole} onChange={this.onChangeAppUserRole}

                                    />


                                    <input type="submit" name="" value="Sign Up" onClick={this.handleRegister}/>


                            </div>
                            </div>
                        )}

                        {this.state.message && (
                            <div className="form-group">
                                <div
                                    className={
                                        this.state.successful
                                            ? "alert alert-success"
                                            : "alert alert-danger"
                                    }
                                    role="alert"
                                >
                                    {this.state.message}
                                </div>
                            </div>
                        )}
                        <CheckButton
                            style={{ display: "none" }}
                            ref={c => {
                                this.checkBtn = c;
                            }}
                        />
                    </Form>

            </div>
            </div>
            </div>

        );
    }
}
export default Register;