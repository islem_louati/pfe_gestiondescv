package com.example.demo.data;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDBRepository extends JpaRepository<FileDB, Integer> {
	Boolean existsByname(String name);
	  Optional<FileDB> findByname(String name);
}
