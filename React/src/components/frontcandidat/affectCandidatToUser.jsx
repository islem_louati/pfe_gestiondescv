import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { Select } from "@material-ui/core";
import CandidatService from "../../services/CandidatService";
import axios from "axios";
import authHeader from "../../services/auth-header";
import userService from "../../services/user.service";
import authService from "../../services/auth.service";

const options = [
  { value: 'done', label: 'done' },
  { value: 'INNACTIF', label: 'INNACTIF' },

];
const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
const invalide = value => {
    if(! value.match(/^([a-zA-Z ]+)$/)) {
        return (
            <div className="alert alert-danger" role="alert">
                Pseudo invalide!
            </div>
        );
    }
};



const vusername = value => {
    if (value.length < 3 || value.length > 15) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const DATEVALIDATE = value => {    
  const date1 = new Date('1980-06-01')
    if(value< date1){
       return  (
          
            <div className="alert alert-danger" role="alert">
                The username must be bed 20 characters.
            </div>
    );

}
  
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class affectCandidattoUser extends Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeAdresse = this.onChangeAdresse.bind(this);
        this.onChangeEtat=this.onChangeEtat.bind(this);

        this.state = {
            id: this.props.match.params.id,
            fullName: '',
            date: '',
            tel: '',
            adresse:'',
            etatc:[],
            dBFile:'',
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            fullName: e.target.value
        });
    }
    changetelHandler= (event) => {
        this.setState({tel: event.target.value});
    }

    onChangeEmail(e) {
        this.setState({
            date: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            tel: e.target.value
        });
    }
    onChangeAdresse(e) {
        this.setState({
            adresse: e.target.value
        });
    }
    onChangeEtat(e) {
        this.setState({
            etatc: e.target.value
        });
    }
 
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            CandidatService.getcandidats(this.state.id).then( (res) =>{
                let candidat = res.data;
                this.setState({fullName: candidat.fullName,
                    date: candidat.date,
                    tel: candidat.tel,
                    adresse: candidat.adresse,
                    etatc :candidat.etatc
                });
            });
        }
    }
    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();


        let candidat = {fullName: this.state.fullName, date: this.state.date,tel:this.state.tel,adresse: this.state.adresse, etatc:this.state.etatc};
        console.log('candidat => ' + JSON.stringify(candidat));
        console.log(candidat)
     

        if (this.checkBtn.context._errors.length === 0) {
 const current= authService.getCurrentUser();

                    axios.post(`http://localhost:8087/SpringMVC/candidats/create/${current.id}`, candidat, { headers: authHeader() })
                  
              
                 this.props.history.push("/profilee");
                
           
                }
           
    }
        
    

    render() {
        return (
        <body>
     
  <div className="col-md-12" style={{marginLeft: '18rem'}}>
<div class="main-body">
<div class="page-wrapper">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>ajouter Candidat</h5>
                                        </div>
                                        <div class="card-body">


                                            <div class="row">
                                                <div class="col-md-6">
                              <Form
                                  onSubmit={this.handleRegister}
                                  ref={c => {
                                      this.form = c;
                                  }}
                              >
                                  {!this.state.successful && (
                                      <div>
                                          
                                          <div className="form-group">
                            <label htmlFor="username">Username</label>
                                <Input type="text" className="form-control" name="username"  value={this.state.fullName} onChange={this.onChangeUsername}
                                   maxlength="15"
                                   validations={[required,vusername,invalide]}/>
                                             </div>
                                             <div className="form-group">
                            <label htmlFor="date">date</label>
                                <Input type="date" className="form-control" id="date" name="date"  
                                  value={this.state.date}
                                  min="1950-06-11" max="2022-06-06"
                                  onChange={this.onChangeEmail}
                                  validations={[required]}/>

                                  
     
                                             </div>
                                             <div className = "form-group">
                                        <label> tel: </label>
                                        <Input placeholder="tel" name="tel" type="Number" className="form-control"
                                               value={this.state.tel} onChange={this.changetelHandler}/>
                                    </div>
                                             <div className="form-group">
                            <label htmlFor="adresse">Adresse</label>
                                <Input type="text" className="form-control" name="adresse" 
                                 value={this.state.adresse}
                                
                                 onChange={this.onChangeAdresse}
                                 validations={[required,vusername]}/>
                                             </div>                                        
                                         
                                             <div className="form-group" >
                                            <select onChange={(ddl)=>{this.setState({etatc:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--Etat--</option>
                                                <option value="done">done</option>
                                                <option value="INNACTIF">INNACTIF</option>
                                            </select>
</div>

                                          <div className="form-group">
                                              <button className="btn btn-primary btn-block" onClick={this.handleRegister} >ajouter</button>
                                          </div>
                                      </div>
                                  )}

                                  {this.state.message && (
                                      <div className="form-group">
                                          <div
                                              className={
                                                  this.state.successful
                                                      ? "alert alert-success"
                                                      : "alert alert-danger"
                                              }
                                              role="alert"
                                          >
                                              {this.state.message}
                                          </div>
                                      </div>
                                  )}
                                  <CheckButton
                                      style={{ display: "none" }}
                                      ref={c => {
                                          this.checkBtn = c;
                                      }}
                                  />
                              </Form>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                          </div>
                      </div>

</body>

        );
    }
}
export default affectCandidattoUser;
