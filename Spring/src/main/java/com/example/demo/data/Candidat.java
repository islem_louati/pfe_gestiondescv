package com.example.demo.data;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
public class Candidat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int CandidatId;
	private String fullName;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date date;
	private String Adresse;
	private String Email;
	private int tel;
	@Enumerated(EnumType.STRING)
	private EtatCandidat etatc;
	@OneToOne(mappedBy = "candidat")
	@JsonIgnore
	private Mission mission;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", referencedColumnName = "id")
	private FileDB dBFile;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "commentaireId", referencedColumnName = "commentaireId")
	private Commentaire commentaire;
	@OneToOne(mappedBy = "candidat")
	@JsonIgnore
	private AppUser appuser;

	public Candidat(int candidatId, String fullName, Date date, String adresse, String email, int tel,
			EtatCandidat etatc) {
		super();
		CandidatId = candidatId;
		this.fullName = fullName;
		this.date = date;
		Adresse = adresse;
		Email = email;
		this.tel = tel;
		this.etatc = etatc;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public FileDB getdBFile() {
		return dBFile;
	}

	public void setdBFile(FileDB dBFile) {
		this.dBFile = dBFile;
	}

	public Commentaire getCommentaire() {
		return commentaire;
	}
	

	public AppUser getAppuser() {
		return appuser;
	}

	public void setAppuser(AppUser appuser) {
		this.appuser = appuser;
	}

	public void setCommentaire(Commentaire commentaire) {
		this.commentaire = commentaire;
	}

	public int getCandidatId() {
		return CandidatId;
	}

	public void setCandidatId(int candidatId) {
		CandidatId = candidatId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}
	

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}
	


	

	public EtatCandidat getEtatc() {
		return etatc;
	}

	public void setEtatc(EtatCandidat etatc) {
		this.etatc = etatc;
	}



	public Candidat( String fullName, Date date, String adresse, int tel, EtatCandidat etatc,
			FileDB dBFile) {
		super();
		
		this.fullName = fullName;
		this.date = date;
		Adresse = adresse;
		this.tel = tel;
		this.etatc = etatc;
		this.dBFile = dBFile;
	}

	public Candidat(String fullName, Date date, String adresse, int tel, EtatCandidat etatc, FileDB dBFile,
			Commentaire commentaire) {
		super();
		this.fullName = fullName;
		this.date = date;
		Adresse = adresse;
		this.tel = tel;
		this.etatc = etatc;
		this.dBFile = dBFile;
		this.commentaire = commentaire;
	}

	public Candidat(int candidatId, String fullName, Date date, String adresse, int tel) {
		super();
		CandidatId = candidatId;
		this.fullName = fullName;
		this.date = date;
		Adresse = adresse;
		this.tel = tel;
	}



	

	public Candidat(int candidatId, String fullName, Date date, String adresse, int tel, EtatCandidat etatc) {
		super();
		CandidatId = candidatId;
		this.fullName = fullName;
		this.date = date;
		Adresse = adresse;
		this.tel = tel;
		this.etatc = etatc;
	}
	

	public Candidat(int candidatId, String fullName, Date date, String adresse, int tel, EtatCandidat etatc,
			Mission mission) {
		super();
		CandidatId = candidatId;
		this.fullName = fullName;
		this.date = date;
		Adresse = adresse;
		this.tel = tel;
		this.etatc = etatc;
		this.mission = mission;
	
	}

	public Candidat() {
		super();
	}
	

}
