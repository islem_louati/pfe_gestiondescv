import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";


import ListCandidatComponent from "./compnent/ListCandidatComponent";
import BoardAdminComponent from "./compnent/BoardAdminComponent";

import LoginComponent from "./compnent/loginComponent";
import registreComponent from "./compnent/registreComponent";
import ProfileComponent from "./compnent/profileComponent";
import CreateCandidatComponent from "./compnent/CreateCandidatComponent";

import ListMissionTermine from "./compnent/ListMissionTermine";
import CreateMissionComponent from "./compnent/CreateMissionComponent";
import UploadFiles from "./compnent/upload-files.component";






class App extends Component {



    render() {


        return (

            <div>


                    <div className="container">
                        <BrowserRouter>
                            <Switch>
                                <Route path= "/user" component={ListCandidatComponent}/>
                                <Route path= "/admin" component={BoardAdminComponent}/>
                                <Route path="/login" component={LoginComponent}/>
                                <Route path="/registre" component={registreComponent}/>
                                <Route path="/profile" component={ProfileComponent}/>
                                <Route path="/candidat" component={ListCandidatComponent}/>
                                <Route path="/add" component={CreateCandidatComponent}/>
                                <Route path="/missions" component={ListMissionTermine}/>
                                <Route path="/add-mission" component={CreateMissionComponent}/>
                                <Route path="/upload" component={UploadFiles}/>



                            </Switch>
                        </BrowserRouter>


                    </div>





            </div>


        );
    }
}

export default App;
