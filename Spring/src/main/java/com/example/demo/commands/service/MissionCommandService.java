package com.example.demo.commands.service;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.demo.commands.CreateMissionCommand;
import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.Commentaire;
import com.example.demo.data.CreateCandidatRequest;
import com.example.demo.data.CreateMissionRequest;
import com.example.demo.data.Etat;
import com.example.demo.data.EtatCandidat;
import com.example.demo.data.Mission;
import com.example.demo.data.Missionepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
//@RequiredArgsConstructor
@Component
public class MissionCommandService {
	private static final Logger l = LogManager.getLogger(MissionCommandService.class);
	private final CandidatRepository rep;
	private final Missionepository repository;

	public MissionCommandService(Missionepository repository, CandidatRepository rep) {
		this.rep = rep;
		this.repository = repository;
	}

	public Mission createAccount(Mission createAccountRequest) {

		l.debug("Handling a Bank Account creation command {}");
		// Mission m ;

		createAccountRequest.getMissionId();
		createAccountRequest.getDescription();
		createAccountRequest.getDatedebut();
		createAccountRequest.getDatefin();
		createAccountRequest.getEtat();

		createAccountRequest.setCandidat(null);

		return this.repository.save(createAccountRequest);

	}

    public void updateMission(Mission mission,int missionId) {
    	Mission m = repository.findById(missionId).orElse(null);
    	 System.out.println(m+"aze");
    	 Candidat c =m.getCandidat();
    	
   if (m != null)
   {
	  
	   System.out.println(m+"aze");
	   mission.setMissionId(missionId);
	 
       this.repository.save(mission);
		
   }
   }
    public void deletemission(int missionId) {
    	Mission m = repository.findById(missionId).orElse(null);
    
 
   if (m != null)
   {
	   
       this.repository.deleteById(missionId);
		
   }}
	public Mission affectcandidatToMission(int missionId, int candidatId) {
		l.debug("Handling a Bank Account creation command {}");
		Candidat candidat = rep.findById(candidatId).orElse(null);
		Mission mission = repository.findById(missionId).orElse(null);
		// Mission m ;
		if (candidat == null && mission !=null)
			mission.getMissionId();

		mission.setCandidat(candidat);

		return this.repository.save(mission);

	}
	public Mission DesaffectcandidatFromMission(int missionId) {
		l.debug("Handling a Bank Account creation command {}");
		Mission mission = repository.findById(missionId).orElse(null);
		// Mission m ;
		if ( mission !=null)
			mission.getMissionId();

		mission.setCandidat(null);

		return this.repository.save(mission);

	}

	// @Scheduled(cron = "*/10 * * * * *")
	// @Scheduled(cron="*/5 * * * * ?")
	public void ActifCandidat() {
		for (Mission m : repository.findAll()) {
			if (m.getCandidat() != null) {

				if (m.getCandidat().getEtatc() != m.getCandidat().getEtatc().done) {
					m.getMissionId();
					m.getCandidat().setEtatc(m.getCandidat().getEtatc().done);
					m.setEtat(Etat.EN_COUR);
					this.repository.save(m);

				}
			}
			else
			{
				m.setEtat(Etat.NON_AFFECTE);
				this.repository.save(m);
			}

		}
	}
	// @Scheduled(cron = "*/10 * * * * *")
	// @Scheduled(cron="*/5 * * * * ?")

	public void missiontermine() {

		for (Mission m : repository.findAll()) {

			Date now = new Date();

			if (new Date(m.getDatefin().getYear(), m.getDatefin().getMonth(), m.getDatefin().getDate())
					.before(new Date(now.getYear(), now.getMonth(), now.getDate())))

			{
				l.info("inf" + m.getEtat());
				m.setEtat(Etat.TERMINE);
				repository.save(m);

			}
		
			
		}

	}




}
