import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Mission from '../services/Mission';
import authHeader from '../services/auth-header';


class missionNonAffecter extends Component {
 
    constructor(props) {
        super(props)

        this.state = {
            id:this.props.match.params.id,
            missions: []
        }
      //  this.addCandidat = this.addCandidat.bind(this);
    }
    viewMission(id){
        console.log(id)
        this.props.history.push(`/affect/${id}`);
        var i = id;
        console.log(i+"zz")
    }

 getcandidat(missionId){
     Mission.getmissionsById(missionId);
     console.log(missionId+"mm")
 }
  deletemissiont(id){
    axios.delete(`http://localhost:8087/SpringMVC/missions/delete/${id}`,{ headers: authHeader() })
   
    window.alert("confirmer la supression ?")
    console.log(id)
    this.props.history.push(`/missions`);
  }
  updatemissiont(id) {
    {
        
      this.props.history.push(`/updatemission/${id}`);
  }}
    editMission(id){
        
        this.props.history.push(`/affect/${id}`);

    }
    componentDidMount() {
        
        Mission.getmissionNonAffecter().then((res) =>{
            this.setState(({missions: res.data}));
        });
    }

    render() {
        return (
            <div>
               <body>
               <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    
    
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>



        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">login</i>
            </div>
            <span class="nav-link-text ms-1" >Profile</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/registre">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">assignment</i>
            </div>
            <span class="nav-link-text ms-1">Comment</span>
          </a>
        </li>
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
 
  </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps ps--active-y">
   
       <div class="container-fluid py-4">
       
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '-2073px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '2073px', height: '724px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '536px', height: '187px'}}></div></div>
    </main>
    <div class="fixed-plugin ps">
       
        <div class="card shadow-lg">
          <div class="card-header pb-0 pt-3">
            <div class="float-start">
              <h5 class="mt-3 mb-0">Material UI Configurator</h5>
              <p>See our dashboard options.</p>
            </div>
            <div class="float-end mt-4">
              <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                <i class="material-icons">clear</i>
              </button>
            </div>

          </div>
          <hr class="horizontal dark my-1"></hr>
          <div class="card-body pt-sm-3 pt-0">

            <div>
              <h6 class="mb-0">Sidebar Colors</h6>
            </div>
            <a href="javascript:void(0)" class="switch-trigger background-color">
              <div class="badge-colors my-2 text-start">
                <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
              </div>
            </a>

            <div class="mt-3">
              <h6 class="mb-0">Sidenav Type</h6>
              <p class="text-sm">Choose between 2 different sidenav types.</p>
            </div>
            <div class="d-flex">
              <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
            </div>
            <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

            <div class="mt-3 d-flex">
              <h6 class="mb-0">Navbar Fixed</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-3"></hr>
            <div class="mt-2 d-flex">
              <h6 class="mb-0">Light / Dark</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-sm-4"></hr>
            <a class="btn btn-outline-dark w-100" href="">View documentation</a>

            <div class="w-100 text-center">
              <span></span>
              <h6 class="mt-3">Thank you for sharing!</h6>
              <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
              </a>
              <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
              </a>

            </div>
          </div>
        </div>

      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
    </body>
                <ul class="nav nav-tabs col-12">
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/missions">tous les missions</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="/missionterminer">mission terminer</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="/missionnonaffecter" tabindex="-1" aria-disabled="true">mission non affecter</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="/mission" tabindex="-1" aria-disabled="true">mission en cour</a>
  </li>
</ul>
       
<div class="row mrgn">
                 <div class="col-12">
      <table className = "table table-striped table-bordered">

                    <thead>
                    <tr>
                        <th> description</th>
                        <th> datedebut</th>
                        <th> datefin</th>
                        <th>etat</th>
                        <th>candidat</th>


                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.missions.map(
                            mission =>
                                <tr key = {mission.missionId} onClick={ () =>  this.candidatsimilaire(mission.missionId)}>
                                    <td> { mission.description} </td>
                                    <td> {mission.datedebut}</td>
                                    <td> {mission.datefin}</td>
                                    <td> {mission.etat}</td>
                                 
                                    { mission.candidat === null ? (
                                        <td onClick={ () => this.viewMission(mission.missionId)}>affecter candidat </td> ): (<td>{mission.candidat.fullName}</td>)
}
<button onClick={ () => this.deletemissiont(mission.missionId)} >supprimer
                                                                   </button> 
                                                                   <button onClick={ () => this.updatemissiont(mission.missionId)} >modifier
                                                                   </button> 
                                </tr>
                        )
                    }
                    </tbody>
                </table>
                <a href="/add-mission" style={{marginLeft: "10px"}}  className="btn btn-info">ajouter Mission </a>
            </div>
            </div>
        </div>
    );
}


}
export default missionNonAffecter;