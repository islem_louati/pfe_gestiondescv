import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { Select } from "@material-ui/core";
import CandidatService from "../services/CandidatService";
import PhoneInput from "react-phone-input-2";
import ReactPhoneInput from "react-phone-input-2"
import authHeader from "../services/auth-header";
import axios from "axios";
const options = [
  { value: 'done', label: 'done' },
  { value: 'INNACTIF', label: 'INNACTIF' },

];
const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
const invalide = value => {
    if(! value.match(/^([a-zA-Z]+)$/)) {
        return (
            <div className="alert alert-danger" role="alert">
                Pseudo invalide!
            </div>
        );
    }
};



const vusername = value => {
    if (value.length < 3 || value.length > 15 ) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const DATEVALIDATE = value => {    
  const date1 = new Date('1980-06-01')
    if(value< date1){
       return  (
          
            <div className="alert alert-danger" role="alert">
                The username must be bed 20 characters.
            </div>
    );

}
  
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class CreateCandidat extends Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeEmaill = this.onChangeEmaill.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeAdresse = this.onChangeAdresse.bind(this);
        this.onChangeEtat=this.onChangeEtat.bind(this);

        this.state = {
            id: this.props.match.params.id,
            fullName: '',
            date: '',
            email:'',
            tel: '',
            adresse:'',
            etatc:[],
            dBFile:'',
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            fullName: e.target.value
        });
    }

    changetelHandler= (event) => {
        this.setState({tel: event.target.value});
    }

    onChangeEmail(e) {
        this.setState({
            date: e.target.value
        });
    }
    onChangeEmaill(e) {
      this.setState({
        email: e.target.value
      });
  }
    onChangePassword(e) {
        this.setState({
            tel: e.target.value
        });
    }
    onChangeAdresse(e) {
        this.setState({
            adresse: e.target.value
        });
    }
    onChangeEtat(e) {
        this.setState({
            etatc: e.target.value
        });
    }
 
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            CandidatService.getcandidats(this.state.id).then( (res) =>{
                let candidat = res.data;
                this.setState({fullName: candidat.fullName,
                    date: candidat.date,
                   
                    tel: candidat.tel,
                    adresse: candidat.adresse,
                    etatc :candidat.etatc,
                    email: candidat.email
                });
            });
        }
    }
 
    handleRegister(e) {
        

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        e.preventDefault();
        let candidats = {fullName: this.state.fullName, date: this.state.date,tel:this.state.tel,adresse: this.state.adresse, etatc:this.state.etatc,email: this.state.email};
        console.log('candidats => ' + JSON.stringify(candidats));
        console.log(candidats)
        if (this.checkBtn.context._errors.length === 0) {
        axios.post(`http://localhost:8087/SpringMVC/candidats/create`, candidats, { headers: authHeader() })
        this.props.history.push(`/candidat`);
      }}

    render() {
        return (
        <body>
                <body>
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/listnonenable">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">user non valider</span>
          </a>
        </li>


        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1" >profile</span>
          </a>
        </li>
      
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
  </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps ps--active-y">
   
       <div class="container-fluid py-4">
       
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '-2073px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '2073px', height: '724px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '536px', height: '187px'}}></div></div>
    </main>
    <div class="fixed-plugin ps">
     
        <div class="card shadow-lg">
          <div class="card-header pb-0 pt-3">
            <div class="float-start">
              <h5 class="mt-3 mb-0">Material UI Configurator</h5>
              <p>See our dashboard options.</p>
            </div>
            <div class="float-end mt-4">
              <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                <i class="material-icons">clear</i>
              </button>
            </div>

          </div>
          <hr class="horizontal dark my-1"></hr>
          <div class="card-body pt-sm-3 pt-0">

            <div>
              <h6 class="mb-0">Sidebar Colors</h6>
            </div>
            <a href="javascript:void(0)" class="switch-trigger background-color">
              <div class="badge-colors my-2 text-start">
                <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
              </div>
            </a>

            <div class="mt-3">
              <h6 class="mb-0">Sidenav Type</h6>
              <p class="text-sm">Choose between 2 different sidenav types.</p>
            </div>
            <div class="d-flex">
              <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
            </div>
            <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

            <div class="mt-3 d-flex">
              <h6 class="mb-0">Navbar Fixed</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-3"></hr>
            <div class="mt-2 d-flex">
              <h6 class="mb-0">Light / Dark</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-sm-4"></hr>
            <a class="btn btn-outline-dark w-100" href="">View documentation</a>

            <div class="w-100 text-center">
              <span></span>
              <h6 class="mt-3">Thank you for sharing!</h6>
              <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
              </a>
              <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
              </a>

            </div>
          </div>
        </div>

      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
    </body>
  <div className="col-md-12" style={{marginLeft: '18rem'}}>
<div class="main-body">
<div class="page-wrapper">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>ajouter Candidat</h5>
                                        </div>
                                        <div class="card-body">


                                            <div class="row">
                                                <div class="col-md-6">
                                                <Form
                                  onSubmit={this.handleRegister}
                                  ref={c => {
                                      this.form = c;
                                  }}
                              >
                                  {!this.state.successful && (
                                      <div>
                                          
                                          <div className="form-group">
                            <label htmlFor="username">Username</label>
                                <Input type="text" className="form-control" name="username"  value={this.state.fullName} onChange={this.onChangeUsername}
                                   maxlength="15"
                                   validations={[required,vusername,invalide]}/>
                                             </div>
                                             <div className="form-group">
                            <label htmlFor="date">date</label>
                                <Input type="date" className="form-control" id="date" name="date"  
                                  value={this.state.date}
                                  min="1950-06-11" max="2022-06-06"
                                  onChange={this.onChangeEmail}
                                  validations={[required]}/>

                                  
     
                                             </div>
                                        
                                             <div className = "form-group">
                                        <label> tel: </label>
                                        <Input placeholder="tel" name="tel" type="Number" className="form-control"
                                               value={this.state.tel} onChange={this.changetelHandler}/>
                                    </div>
                                             <div className="form-group">
                            <label htmlFor="adresse">Adresse</label>
                                <Input type="text" className="form-control" name="adresse" 
                                 value={this.state.adresse}
                                
                                 onChange={this.onChangeAdresse}
                                 validations={[required,vusername]}/>
                                             </div>                                        
                                         
                                             <div className="form-group" >
                                            <select onChange={(ddl)=>{this.setState({etatc:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--Etat--</option>
                                                <option value="done">done</option>
                                                <option value="INNACTIF">INNACTIF</option>
                                            </select>
</div>
<div className = "form-group">
                                        <label> email: </label>
                                        <input type="text" placeholder="email" name="email" className="form-control"
                                               value={this.state.email} onChange={this.onChangeEmaill}/>
                                    </div>
                                          <div className="form-group">
                                              <button className="btn btn-primary btn-block" onClick={this.handleRegister} >ajouter</button>
                                          </div>
                                      </div>
                                  )}

                                  {this.state.message && (
                                      <div className="form-group">
                                          <div
                                              className={
                                                  this.state.successful
                                                      ? "alert alert-success"
                                                      : "alert alert-danger"
                                              }
                                              role="alert"
                                          >
                                              {this.state.message}
                                          </div>
                                      </div>
                                  )}
                                  <CheckButton
                                      style={{ display: "none" }}
                                      ref={c => {
                                          this.checkBtn = c;
                                      }}
                                  />
                              </Form>
 
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                          </div>
                      </div>

</body>

        );
    }
}
export default CreateCandidat;
