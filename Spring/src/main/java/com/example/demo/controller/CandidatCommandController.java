package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import javax.validation.Valid;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commands.CreateMissionCommand;
import com.example.demo.commands.service.CandidatCommandService;
import com.example.demo.commands.service.MissionCommandService;
import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.CreateCandidatRequest;
import com.example.demo.data.EtatCandidat;
import com.example.demo.data.FileDB;
import com.example.demo.data.FileDBRepository;
import com.example.demo.data.Mission;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/candidats")
public class CandidatCommandController {
	private final CandidatCommandService candidatCommandService ;
	private final MissionCommandService ms;
	@Autowired
	FileDBRepository filerepo;
	@Autowired
	CandidatRepository cand;
	public CandidatCommandController(CandidatCommandService candidatCommandService,MissionCommandService ms) {
		this.candidatCommandService = candidatCommandService;
		this.ms=ms;
	}
	@GetMapping("/candidatbyfile")
	public Set<Candidat> getcanididatbyfile(){
		List<Candidat> c = cand.findAll();
		Set<Candidat> candidatlist = new HashSet<>();
		for (Candidat candidat : c) {
			if(candidat.getdBFile() != null)
				candidatlist.add(candidat);
		}
		return candidatlist;
		
	}

	@GetMapping("/allfiles")
	public Set<Candidat> getListFile() throws IOException {
		Set<Candidat> fileList = new HashSet<>();
		
		String l = "islem";
		String message = "";
		String s = "";
	
		
	
		boolean found = true;
		File dir = new File("uploads/files");
		File[] files = dir.listFiles();
		PDDocument document = null;
		for (FileDB ff : filerepo.findAll()) {

		for (File file : files) {
			if (file.isFile()) {
			if (file.getName().equals(ff.getName())) {
				
					BufferedReader inputStream = null;
					String line;

					FileInputStream fis = new FileInputStream(file);
					document = PDDocument.load(ff.getData());
					

					PDFTextStripper pdfTextStripper = new PDFTextStripper();
					pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire
														// document
					pdfTextStripper.getEndPage(); // comment this line if you want to read the entire document
					String docText = pdfTextStripper.getText(document);
					
						fileList.add(ff.getCandidat());
						
					

					
					document.close();}}
		}}
return fileList;
		}
	@PostMapping("/create/{id}")
  //  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale') or hasRole('ROLE_Candidat')")
	public ResponseEntity<String> createAccount(@RequestBody Candidat request, @PathVariable Long id) {
		try {
			Candidat response = candidatCommandService.createAccount(request,id);

			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	@PostMapping("/create")
	  //  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale') or hasRole('ROLE_Candidat')")
		public ResponseEntity<String> createcANDIDAT(@RequestBody Candidat request) {
			try {
				Candidat response = candidatCommandService.createCandidat(request);

				return new ResponseEntity<>( HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	@PutMapping("/update/{candidatId}")
	  //  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale') or hasRole('ROLE_Candidat')")
		public ResponseEntity<String> updateaccount(@RequestBody Candidat request,  @PathVariable int candidatId ) {
			try {
				Candidat response = candidatCommandService.updateCandidat(request,candidatId);

				return new ResponseEntity<>( HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	@DeleteMapping("/delete/{candidatId}")
		public ResponseEntity<String> deleteaccount(@PathVariable int candidatId ) {
			try {
			    candidatCommandService.deleteCandidat(candidatId);

				return new ResponseEntity<>( HttpStatus.ACCEPTED);
			} catch (Exception e) {
				return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	@PostMapping("/affect/{missionId}/{candidatId}")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale')")
	public ResponseEntity<String> affectcandidatToMission(@PathVariable int missionId, @PathVariable int candidatId  ) {
		try {
			Mission response = ms.affectcandidatToMission(missionId,candidatId);

			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
		@PutMapping("/affectfile/{CandidatId}/{id}")
		public void affectFileToCandidat(@PathVariable("CandidatId") int CandidatId, @PathVariable("id") int id) {

				candidatCommandService.AffectCvToCandidat(CandidatId,id) ;
			
			}
	}


