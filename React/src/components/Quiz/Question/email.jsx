import React, {Component} from 'react';

import { Link } from 'react-router-dom';

import axios from 'axios';
import CandidatService from '../services/CandidatService';
import Mission from '../services/Mission';
import userService from '../services/user.service';
import authHeader from '../services/auth-header';


class ListCandidatComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
          
          id:this.props.match.params.id,

            candidats: [],
            missions:{},
           
        }
        this.addCandidat = this.addCandidat.bind(this);
        this.editCandidat = this.editCandidat.bind(this);
        this.deletecandidat=this.deletecandidat.bind(this);
    }
   
    viewCandidat(id){
   
      Mission.getmissionsById(this.state.id).then(res=>{
        this.setState({mission:res.data});
    console.log(res.data.missionId)
   
    axios.post(`http://localhost:8087/SpringMVC/candidats/affect/${res.data.missionId}/${id}`)
    .then((response) => {
      this.setState({ missions: response.data });
    })
   window.alert("candidat affecter")
      
  }) }
  updatecandidat(id) {
    {
      
        
      this.props.history.push(`/update/${id}`);
  }
    
}

deletecandidat(id){
  axios.delete(`http://localhost:8087/SpringMVC/candidats/delete/${id}`,{ headers: authHeader() })
 
  window.alert("confirmer la supression ?")
  console.log(id)
  window.location.reload();
}


componentDidMount() {
        userService.getAdminBoard().then((res) =>{
        this.setState(({candidats: res.data}));
        console.log(res.data)
        });
       
        
}
    addCandidat(){
        this.props.history.push('/create/_add');
    }
    
    

    editCandidat(id){
        
      this.props.history.push(`/upload/${id}`);
  }

  
  affectComment(id){
     
    this.props.history.push(`/comment/${id}`);
}




}

export default ListCandidatComponent;
