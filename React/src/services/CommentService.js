import axios from 'axios';
import authHeader from './auth-header';


const  Comment_API_BASE_URL = "http://localhost:8087/SpringMVC/commentaire/get-commentaire";
const  AllComment_API_BASE_URL = "http://localhost:8087/SpringMVC/commentaire/getallcommentaire";
const Create_Comment_To_Candidat ="http://localhost:8087/SpringMVC/Comment/create"
class Comment {
getCommentById(commentaireId) {
    console.log(commentaireId)
    return axios.get(Comment_API_BASE_URL+'/'+commentaireId,{ headers: authHeader() });
   
}
affectComment(comment,candidatId){
    console.log(candidatId)
    console.log(comment)
    return axios.post(Create_Comment_To_Candidat+'/'+candidatId,comment,{ headers: authHeader() });
}

}
export default new Comment();