import axios from 'axios';


const  Mission_API_BASE_URL = "http://localhost:8087/SpringMVC/mission/missiontermine";
const  Missions_API_BASE_URL = "http://localhost:8087/SpringMVC/mission/missions";
const  MissionCREATE_API_BASE_URL = "http://localhost:8087/SpringMVC/missions/create";
class Mission {
    getmissionTermine() {
        return axios.get(Mission_API_BASE_URL);
    }
    getmissions() {
        return axios.get(Missions_API_BASE_URL);
    }
    createMission(mission){
        return axios.post(MissionCREATE_API_BASE_URL, mission);
    }
}
export default  new Mission()