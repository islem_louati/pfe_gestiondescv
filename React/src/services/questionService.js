import axios from 'axios';
import authHeader from './auth-header';


const  CANDIDAT_API_BASE_URL = "http://localhost:8087/SpringMVC/questions/q";
const  CANDIDATCREATE_API_BASE_URL = "http://localhost:8087/SpringMVC/questions/create";
const  update_API_BASE_URL = "http://localhost:8087/SpringMVC/questions/update";

    class QuestionService{
        getquestion(){
            return axios.get(CANDIDAT_API_BASE_URL)
        }
        createquestion(question){
            return axios.post(CANDIDATCREATE_API_BASE_URL,question)

        }
        updatequestion(question,id){
            console.log(id)
            console.log(question)
            return axios.put(update_API_BASE_URL+'/'+id,question);
        }
    }
    export default new QuestionService()