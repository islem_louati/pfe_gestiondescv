import React, {Component} from 'react';
import mission from "../Service/Mission";

class ListMissionTermine extends Component {
    constructor(props) {
        super(props)

        this.state = {
            missions: []
        }
      //  this.addCandidat = this.addCandidat.bind(this);
    }

    componentDidMount() {
        mission.getmissionTermine().then((res) =>{
            this.setState(({missions: res.data}));
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Employees List</h2>
                <div className = "row">

                </div>
                <br></br>

                <div className = "row">
                    <table className = "table table-striped table-bordered">

                        <thead>
                        <tr>
                            <th> description</th>
                            <th> datedebut</th>
                            <th> datefin</th>
                            <th>etat</th>
                            <th>candidat</th>


                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.missions.map(
                                mission =>
                                    <tr key = {mission.missionId}>
                                        <td> { mission.description} </td>
                                        <td> {mission.datedebut}</td>
                                        <td> {mission.datefin}</td>
                                        <td> {mission.etat}</td>
                                        <td>{mission.candidat.fullName}</td>

                                        <td>

                                            <button style={{marginLeft: "10px"}} onClick={ () => this.viewMission(mission.missionId)} className="btn btn-info">View </button>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }


}


export default ListMissionTermine;