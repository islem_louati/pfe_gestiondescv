import React, {Component} from 'react';
import ComercialeService from '../services/ComercialeService';

import axios from 'axios';
import authHeader from '../services/auth-header';
import userService from '../services/user.service';
export default class CreateCommercialeByAdminComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            entite_commerciale: '',
        titre: '',
        nom: '',
        prenom: '',
        image: '',
        date_naissance: '',
        ville: '',
        pays: '',
        code_postale: '',
        email: '',
        tel: '',
        adresse: '',
        droits_Speciaux: '',
        expertise: '',
        users: [],
        }
    }
    componentDidMount(){
        if(this.state.id === '_add'){
            return
        }else{
            userService.getUserByname(this.state.id).then( (res) =>{
                let commerciale = res.data;
                this.setState({entite_commerciale: commerciale.entite_commerciale,
                titre: commerciale.titre,
                nom: commerciale.nom,
                prenom: commerciale.prenom,
                image: commerciale.image,
                date_naissance: commerciale.date_naissance,
                ville: commerciale.ville,
                pays: commerciale.pays,
                code_postale: commerciale.code_postale,
                email: commerciale.email,
                tel: commerciale.tel,
                adresse: commerciale.adresse,
                droits_Speciaux: commerciale.droits_Speciaux,
                expertise: commerciale.expertise,
                   

                });
            });
        }
    }
    changeentite_commercialeHandler= (event) => {
        this.setState({entite_commerciale: event.target.value});
    }
    changetitreHandler= (event) => {
        this.setState({titre: event.target.value});
    }
    changenomHandler= (event) => {
        this.setState({nom: event.target.value});
    }
    
    changeprenomHandler= (event) => {
        this.setState({prenom: event.target.value});
    }
    
    changeimageHandler= (event) => {
        this.setState({image: event.target.value});
    }
    changedate_naissanceHandler= (event) => {
        this.setState({date_naissance: event.target.value});
    }
    changevilleHandler= (event) => {
        this.setState({ville: event.target.value});
    }
    changepaysHandler= (event) => {
        this.setState({pays: event.target.value});
    }
    changecode_postaleHandler= (event) => {
        this.setState({code_postale: event.target.value});
    }
    changeemailHandler= (event) => {
        this.setState({email: event.target.value});
    }
    changetelHandler= (event) => {
        this.setState({tel: event.target.value});
    }
    changeadresseHandler= (event) => {
        this.setState({adresse: event.target.value});
    }
    changedroits_SpeciauxHandler= (event) => {
        this.setState({droits_Speciaux: event.target.value});
    }
    changeexpertiseHandler= (event) => {
        this.setState({expertise: event.target.value});
    }

    savecommerciale = (m) => {
        m.preventDefault();
        let commerciale = {
            entite_commerciale: this.state.entite_commerciale,
            titre: this.state.titre,
            nom: this.state.nom,
            prenom: this.state.prenom,
            image:this.state.image,
            date_naissance:this.state.date_naissance,
            ville:this.state.ville,
            pays:this.state.pays,
            code_postale:this.state.code_postale,
            email:this.state.email,
            tel:this.state.tel,
            adresse:this.state.adresse,
            droits_Speciaux:this.state.droits_Speciaux,
            expertise:this.state.expertise
        };
        console.log('commerciale => ' + JSON.stringify(commerciale));
          

        // step 5
       
        axios.post(`http://localhost:8087/SpringMVC/commerciale/create`, commerciale, { headers: authHeader() })
    }


    
    render() {
      
   
       
    
        return (
            <div>
                 <body>
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/listnonenable">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">user non valider</span>
          </a>
        </li>


        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1" >profile</span>
          </a>
        </li>
      
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
  </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps ps--active-y">
   
       <div class="container-fluid py-4">
       
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '-2073px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '2073px', height: '724px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '536px', height: '187px'}}></div></div>
    </main>
    <div class="fixed-plugin ps">
    
        <div class="card shadow-lg">
          <div class="card-header pb-0 pt-3">
            <div class="float-start">
              <h5 class="mt-3 mb-0">Material UI Configurator</h5>
              <p>See our dashboard options.</p>
            </div>
            <div class="float-end mt-4">
              <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                <i class="material-icons">clear</i>
              </button>
            </div>

          </div>
          <hr class="horizontal dark my-1"></hr>
          <div class="card-body pt-sm-3 pt-0">

            <div>
              <h6 class="mb-0">Sidebar Colors</h6>
            </div>
            <a href="javascript:void(0)" class="switch-trigger background-color">
              <div class="badge-colors my-2 text-start">
                <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
              </div>
            </a>

            <div class="mt-3">
              <h6 class="mb-0">Sidenav Type</h6>
              <p class="text-sm">Choose between 2 different sidenav types.</p>
            </div>
            <div class="d-flex">
              <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
            </div>
            <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

            <div class="mt-3 d-flex">
              <h6 class="mb-0">Navbar Fixed</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-3"></hr>
            <div class="mt-2 d-flex">
              <h6 class="mb-0">Light / Dark</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-sm-4"></hr>
            <a class="btn btn-outline-dark w-100" href="">View documentation</a>

            <div class="w-100 text-center">
              <span></span>
              <h6 class="mt-3">Thank you for sharing!</h6>
              <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
              </a>
              <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
              </a>

            </div>
          </div>
        </div>

      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
    </body>
                <br></br>
                <div className = "container">
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                           
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> entite_commerciale: </label>
                                        <select onChange={(ddl)=>{this.setState({entite_commerciale:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--entite_commerciale--</option>
                                                <option value="AGENCE_EST">AGENCE_EST</option>
                                                <option value="AGENCE_NORD">AGENCE_NORD</option>
                                                <option value="AGENCE_SUD">AGENCE_SUD</option>
                                            </select>
                                    </div>
                                    <div className = "form-group">
                                        <label> disposition_par_rapport_statutAE: </label>
                                        <select onChange={(ddl)=>{this.setState({titre:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--Titre--</option>
                                                <option value="Mme">Mme</option>
                                                <option value="M">M</option>
                                            </select>
                                    </div>
                                    <div className = "form-group">
                                        <label> nom: </label>
                                        <input type="text" placeholder="nom" name="nom" className="form-control"
                                               value={this.state.nom} onChange={this.changenomHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> prenom: </label>
                                        <input type="text" placeholder="prenom" name="prenom" className="form-control"
                                               value={this.state.prenom} onChange={this.changeprenomHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> image: </label>
                                        <input type="text" placeholder="image" name="image" className="form-control"
                                               value={this.state.image} onChange={this.changeimageHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> date_naissance: </label>
                                        <input placeholder="date_naissance" name="date_naissance" className="form-control" type="date"                                               value={this.state.date_relance} onChange={this.changedate_relanceHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> ville: </label>
                                        <input type="text" placeholder="ville" name="ville" className="form-control"
                                               value={this.state.ville} onChange={this.changevilleHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> pays: </label>
                                        <input type="text" placeholder="pays" name="pays" className="form-control"
                                               value={this.state.pays} onChange={this.changepaysHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> code_postale: </label>
                                        <input type="number" placeholder="code_postale" name="code_postale" className="form-control"
                                               value={this.state.code_postale} onChange={this.changecode_postaleHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> email: </label>
                                        <input type="text" placeholder="email" name="email" className="form-control"
                                               value={this.state.email} onChange={this.changeemailHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> tel: </label>
                                        <input type="number" placeholder="tel" name="tel" className="form-control"
                                               value={this.state.tel} onChange={this.changetelHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> adresse: </label>
                                        <input type="text" placeholder="adresse" name="adresse" className="form-control"
                                               value={this.state.adresse} onChange={this.changeadresseHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> droits_Speciaux: </label>
                                        <select onChange={(ddl)=>{this.setState({droits_Speciaux:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--droits_Speciaux--</option>
                                                <option value="Lecture_seule">Lecture_seule</option>
                                                <option value="e_signatures">e_signatures</option>
                                                <option value="Frais_avec_Jenji">Frais_avec_Jenji</option>
                                                <option value="Autorise_edition_des_portals_pages">Autorise_edition_des_portals_pages</option>
                                                
                                            </select>
                                    </div>
                                    <div className = "form-group">
                                        <label> expertise: </label>
                                        <select onChange={(ddl)=>{this.setState({expertise:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--expertise--</option>
                                                <option value="Analyste">Analyste</option>
                                                <option value="Architecte">Architecte</option>
                                                <option value="Chef_de_projet">Chef_de_projet</option>
                                                <option value="Utilisateur_cle">Utilisateur_cle</option>
                                                <option value="Chef_projet_client">Chef_projet_client</option>
                                                <option value="Developpeur">Developpeur</option>
                                                <option value="Comptable">Comptable</option>
                                                <option value="Consultant_fonctionnel">Consultant_fonctionnel</option>
                                                <option value="Testeur">Testeur</option>
                                            </select>
                                    </div>
                                    <div>
                                   
                                  
        
                               </div>  

                                   
                                   
                                    <button className="btn btn-success" onClick={this.savecommerciale}>Save</button>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
    
}