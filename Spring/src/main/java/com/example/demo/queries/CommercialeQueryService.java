package com.example.demo.queries;

import java.util.List;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.AppUserRole;
import com.example.demo.data.Etat;
import com.example.demo.data.Mission;
import com.example.demo.data.Missionepository;
@Service
public class CommercialeQueryService {
	private final AppUserRepository apprep;

	public CommercialeQueryService(AppUserRepository apprep) {
		this.apprep = apprep;
	}

	
	   @QueryHandler
	    public List<AppUser> getuserCommerciale() {
	        return this.apprep.findByAppUserRole(AppUserRole.COMMERCIALE);
	                        
	                    
	    }
	   
}
