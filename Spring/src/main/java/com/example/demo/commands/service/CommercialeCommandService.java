package com.example.demo.commands.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.BeanDefinitionDsl.Role;
import org.springframework.stereotype.Component;

import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.AppUserRole;
import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.CommercialeRepository;
import com.example.demo.data.Entite_commerciale;
import com.example.demo.data.FileDB;
import com.example.demo.data.FileDBRepository;
import com.example.demo.data.Mission;
import com.example.demo.data.Titre;
import com.example.demo.data.comerciale;
import com.example.demo.email.EmailSender;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@RequiredArgsConstructor
@Component
public class CommercialeCommandService {
	 @Autowired
	  private CommercialeRepository comrep;
	 @Autowired
	 private AppUserRepository userrep;
	 @Autowired
	  private  EmailSender emailSender;
	 
	 public Set<comerciale> getcom()
	 {
		 Set<comerciale> fileList = new HashSet<>();
		
		for (comerciale cc : comrep.findAll()) {
			if(cc.getClient() == null && cc.getAppuser() != null)
				
			
			fileList.add(cc);
		}
		return fileList;
	 }
	 
	 public comerciale createAccountCommerciale(comerciale createAccountRequest ,Long id) {
	    	
	   List<AppUser> c = userrep.findByAppUserRole(AppUserRole.COMMERCIALE);
	   AppUser a =userrep.findById(id).orElse(null);
	    			createAccountRequest.getUserId();
	                createAccountRequest.getEntite_commerciale();
	                createAccountRequest.getDroits_Speciaux();
	                createAccountRequest.getTitre();
	                createAccountRequest.getTel();
	                createAccountRequest.getNom();
	                createAccountRequest.getPrenom();
	                createAccountRequest.getImage();
	                createAccountRequest.getDate_naissance();
	                createAccountRequest.getExpertise();
	                createAccountRequest.getAdresse();
	                createAccountRequest.getVille();
	                createAccountRequest.getPays();
	                createAccountRequest.getCode_postale();
	                createAccountRequest.getEmail();
	                createAccountRequest.getTel();
	               // createAccountRequest.setAppUser(a);
	    
	    
	    	    return this.comrep.save(createAccountRequest);
	    
	        }
	 
		public comerciale affectcomercialeTouser(Long id, comerciale createAccountRequest) {
			   AppUser a =userrep.findById(id).orElse(null);
		
			// Mission m ;
			if (a == null )
				createAccountRequest.getUserId();
            createAccountRequest.getEntite_commerciale();
            createAccountRequest.getDroits_Speciaux();
            createAccountRequest.getTitre();
            createAccountRequest.getTel();
            createAccountRequest.getNom();
            createAccountRequest.getPrenom();
            createAccountRequest.getImage();
            createAccountRequest.getDate_naissance();
            createAccountRequest.getExpertise();
            createAccountRequest.getAdresse();
            createAccountRequest.getVille();
            createAccountRequest.getPays();
            createAccountRequest.getCode_postale();
            createAccountRequest.getEmail();
            createAccountRequest.getTel();

            a.setComerciale(createAccountRequest);
            userrep.save(a);
			return this.comrep.save(createAccountRequest);

		}
	 public void createCommercialeByAdmin(comerciale createAccountRequest) {
	    	
		   List<AppUser> c = userrep.findByAppUserRole(AppUserRole.COMMERCIALE);
		    			createAccountRequest.getUserId();
		                createAccountRequest.getEntite_commerciale();
		                createAccountRequest.getDroits_Speciaux();
		                createAccountRequest.getTitre();
		                createAccountRequest.getTel();
		                createAccountRequest.getNom();
		                createAccountRequest.getPrenom();
		                createAccountRequest.getImage();
		                createAccountRequest.getDate_naissance();
		                createAccountRequest.getExpertise();
		                createAccountRequest.getAdresse();
		                createAccountRequest.getVille();
		                createAccountRequest.getPays();
		                createAccountRequest.getCode_postale();
		                createAccountRequest.getEmail();
		                createAccountRequest.getTel();
		            
		              
		    
		    	     this.comrep.save(createAccountRequest);
		    	    String link = "http://localhost:3000/commercialetouser/" + createAccountRequest.getUserId();
		    	       
		            emailSender.sendTocommerciale(
		            		createAccountRequest.getEmail(),
		            		buildEmailtocommerciale(createAccountRequest.getNom(), link));
		        }
	 private String buildEmailtocommerciale(String name, String link) {
	        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
	                "\n" +
	                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
	                "\n" +
	                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
	                "    <tbody><tr>\n" +
	                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
	                "        \n" +
	                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
	                "          <tbody><tr>\n" +
	                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
	                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
	                "                  <tbody><tr>\n" +
	                "                    <td style=\"padding-left:10px\">\n" +
	                "                  \n" +
	                "                    </td>\n" +
	                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
	                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
	                "                    </td>\n" +
	                "                  </tr>\n" +
	                "                </tbody></table>\n" +
	                "              </a>\n" +
	                "            </td>\n" +
	                "          </tr>\n" +
	                "        </tbody></table>\n" +
	                "        \n" +
	                "      </td>\n" +
	                "    </tr>\n" +
	                "  </tbody></table>\n" +
	                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
	                "    <tbody><tr>\n" +
	                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
	                "      <td>\n" +
	                "        \n" +
	                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
	                "                  <tbody><tr>\n" +
	                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
	                "                  </tr>\n" +
	                "                </tbody></table>\n" +
	                "        \n" +
	                "      </td>\n" +
	                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
	                "    </tr>\n" +
	                "  </tbody></table>\n" +
	                "\n" +
	                "\n" +
	                "\n" +
	                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
	                "    <tbody><tr>\n" +
	                "      <td height=\"30\"><br></td>\n" +
	                "    </tr>\n" +
	                "    <tr>\n" +
	                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
	                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
	                "        \n" +
	                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> the admin has been add your profile to commerciale so  Please click on the below link to created an account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Created Now</a> </p></blockquote>\n  <p>See you soon</p>" +
	                "        \n" +
	                "      </td>\n" +
	                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
	                "    </tr>\n" +
	                "    <tr>\n" +
	                "      <td height=\"30\"><br></td>\n" +
	                "    </tr>\n" +
	                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
	                "\n" +
	                "</div></div>";
	    }
		
	 public List<AppUser> getusercommerciale ()
	 {
		return  userrep.findByAppUserRole(AppUserRole.COMMERCIALE);

		
		
}
	 }
