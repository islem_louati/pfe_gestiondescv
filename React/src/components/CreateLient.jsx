import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { Select } from "@material-ui/core";
import CandidatService from "../services/CandidatService";
import ClientService from "../services/ClientService";

const options = [
  { value: 'done', label: 'done' },
  { value: 'INNACTIF', label: 'INNACTIF' },

];
const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
const invalide = value => {
    if(! value.match(/^([a-zA-Z ]+)$/)) {
        return (
            <div className="alert alert-danger" role="alert">
                Pseudo invalide!
            </div>
        );
    }
};



const vusername = value => {
    if (value.length < 3 || value.length > 20 ) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class CreateClientt extends Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.changeNomHandler = this.changeNomHandler.bind(this);
        this.changenumerotelHandler = this.changenumerotelHandler.bind(this);
       

        this.state = {
            id: this.props.match.params.id,
                nom: '',
                numerotel: '',
                description: '',
                adresse: '',
                codepostal:'',
                ville:'',
                pays:'',
                contactname:'',
                contactprenom:'',
                contacttel:'',
               
            successful: false,
            message: ""
        };
    }

    changeNomHandler= (event) => {
        this.setState({nom: event.target.value});
    }

    changenumerotelHandler= (event) => {
        this.setState({numerotel: event.target.value});
    }



    changedescriptionHandler= (event) => {
        this.setState({description: event.target.value});
    }
    changeadresseHandler= (event) => {
        this.setState({adresse: event.target.value});
    }
    changecodepostalHandler= (event) => {
        this.setState({codepostal: event.target.value});
    }
    changevilleHandler= (event) => {
        this.setState({ville: event.target.value});
    }
    changepaysHandler= (event) => {
        this.setState({pays: event.target.value});
    }
    changecontactnameHandler= (event) => {
        this.setState({contactname: event.target.value});
    }
    changecontactprenomHandler= (event) => {
        this.setState({contactprenom: event.target.value});
    }
    changecontacttelHandler= (event) => {
        this.setState({contacttel: event.target.value});
    }


    cancel(){
        this.props.history.push('/missions');
    }

    componentDidMount(){
    
        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            ClientService.getclients(this.state.id).then( (res) =>{
                let client = res.data;
                this.setState({nom: client.nom,
                    numerotel: client.numerotel,
                    description: client.description,
                    adresse: client.adresse,
                    codepostal: client.codepostal,
                    ville: client.ville,
                    pays: client.pays,
                    contactname: client.contactname,
                    contactprenom: client.contactprenom,
                    contacttel: client.contacttel,

                });
            });
        }
    }
    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();
            let client = {nom: this.state.nom, numerotel: this.state.numerotel,description:this.state.description,adresse: this.state.adresse,
                codepostal: this.state.codepostal, ville: this.state.ville,pays: this.state.pays,contactname: this.state.contactname,contactprenom: this.state.contactprenom,
                contacttel: this.state.contacttel };
        if (this.checkBtn.context._errors.length === 0) {

            ClientService.createClient(client
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                    this.props.history.push("/candidat");
                    window.location.reload();
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }

    render() {
        return (
        <body>
                 <body>
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/listnonenable">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">user non valider</span>
          </a>
        </li>


        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1" >profile</span>
          </a>
        </li>
      
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
  </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps ps--active-y">
   
       <div class="container-fluid py-4">
       
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '-2073px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '2073px', height: '724px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '536px', height: '187px'}}></div></div>
    </main>
    <div class="fixed-plugin ps">
     
        <div class="card shadow-lg">
          <div class="card-header pb-0 pt-3">
            <div class="float-start">
              <h5 class="mt-3 mb-0">Material UI Configurator</h5>
              <p>See our dashboard options.</p>
            </div>
            <div class="float-end mt-4">
              <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                <i class="material-icons">clear</i>
              </button>
            </div>

          </div>
          <hr class="horizontal dark my-1"></hr>
          <div class="card-body pt-sm-3 pt-0">

            <div>
              <h6 class="mb-0">Sidebar Colors</h6>
            </div>
            <a href="javascript:void(0)" class="switch-trigger background-color">
              <div class="badge-colors my-2 text-start">
                <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
              </div>
            </a>

            <div class="mt-3">
              <h6 class="mb-0">Sidenav Type</h6>
              <p class="text-sm">Choose between 2 different sidenav types.</p>
            </div>
            <div class="d-flex">
              <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
            </div>
            <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

            <div class="mt-3 d-flex">
              <h6 class="mb-0">Navbar Fixed</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-3"></hr>
            <div class="mt-2 d-flex">
              <h6 class="mb-0">Light / Dark</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-sm-4"></hr>
            <a class="btn btn-outline-dark w-100" href="">View documentation</a>

            <div class="w-100 text-center">
              <span></span>
              <h6 class="mt-3">Thank you for sharing!</h6>
              <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
              </a>
              <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
              </a>

            </div>
          </div>
        </div>

      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
    </body>
  <div className="col-md-12" style={{marginLeft: '18rem'}}>
<div class="main-body">
<div class="page-wrapper">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>ajouter Candidat</h5>
                                        </div>
                                        <div class="card-body">


                                            <div class="row">
                                                <div class="col-md-6">
                              <Form
                                  onSubmit={this.handleRegister}
                                  ref={c => {
                                      this.form = c;
                                  }}
                              >
                                  {!this.state.successful && (
                                     <div class="card-body">
    
    
                                     <div class="row">
                                         <div class="col-md-6">
                
                               <div>
                                 
                  
                         
                                   <div className="form-group">
                       
                                        <label htmlFor="nom">nom </label>
                                       <Input
                                           type="text" className="form-control" 
                                           value={this.state.nom} onChange={this.changeNomHandler}
                                           validations={[required,invalide]}
                                       />
                                   </div>

                                   <div className="form-group">
                                       <label for="numerotel">numero tel </label>
                                       <Input
                                           type="number" className="form-control" 
                                           value={this.state.numerotel} onChange={this.changenumerotelHandler}
                                          
                                          
                                           validations={[required]}

                                       />
                                       
                                   </div>
                                   <div className="form-group">
                                       <label for="description">description </label>
                                       <Input
                                           type="text" className="form-control" 
                                           value={this.state.description} onChange={this.changedescriptionHandler}
                                         
                                          
                                           validations={[required]}

                                       />
                                       
                                   </div>
                                   <div className="form-group">
                                        <label for="adresse">adresse </label>
                                       <Input
                                          type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.adresse}
                                           onChange={this.changeadresseHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                   <div className="form-group">
                                        <label for="codepostal">code postale </label>
                                       <Input
                                          type="number" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.codepostal}
                                           onChange={this.changecodepostalHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                   <div className="form-group">
                                        <label for="ville">ville </label>
                                       <Input
                                          type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.ville}
                                           onChange={this.changevilleHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                   <div className="form-group">
                                        <label for="pays">pays </label>
                                       <Input
                                          type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.pays}
                                           onChange={this.changepaysHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                   <div className="form-group">
                                        <label for="contactname">contact name </label>
                                       <Input
                                          type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.contactname}
                                           onChange={this.changecontactnameHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                   <div className="form-group">
                                        <label for="contactprenom">contact prenom </label>
                                       <Input
                                          type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.contactprenom}
                                           onChange={this.changecontactprenomHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                   <div className="form-group">
                                        <label for="contacttel">contact tel </label>
                                       <Input
                                          type="number" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer numéro du téléphoen"
                                           value={this.state.contacttel}
                                           onChange={this.changecontacttelHandler}
                                           validations={[required]}
                                       />

                                   </div>
                                  
                               


                                   <div className="form-group">
                                       <button className="btn btn-primary btn-block" onClick={this.saveOrUpdateClient} >ajouter</button>
                                   </div>
                               </div>
                           

                   

                 
                 
                       </div>
                       </div>
                       </div>
                                  )}

                                  {this.state.message && (
                                      <div className="form-group">
                                          <div
                                              className={
                                                  this.state.successful
                                                      ? "alert alert-success"
                                                      : "alert alert-danger"
                                              }
                                              role="alert"
                                          >
                                              {this.state.message}
                                          </div>
                                      </div>
                                  )}
                                  <CheckButton
                                      style={{ display: "none" }}
                                      ref={c => {
                                          this.checkBtn = c;
                                      }}
                                  />
                              </Form>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                          </div>
                      </div>

</body>

        );
    }
}
export default CreateClientt;
