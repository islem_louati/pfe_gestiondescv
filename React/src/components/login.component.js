import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import AuthService from "../services/auth.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      username: "",
      password: "",
      loading: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.login(this.state.username, this.state.password).then(
        () => {
          this.props.history.push("/profile");
          window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    return (
     <div>
        <body class="bg-gray-200">
        <main class="main-content mt-0 ps">
   <div class="page-header align-items-start min-vh-100"
      style={{ backgroundimage: 'url("https://images.unsplash.com/photo-1497294815431-9365093b7331?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=1950&amp;q=80")' }}>
       <div class="container my-auto">
               <div class="row">
                 <div class="col-lg-4 col-md-8 col-12 mx-auto">
                   <div class="card z-index-0 fadeIn3 fadeInBottom">
                     <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                       <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                         <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">Sign in</h4>
                         <div class="row mt-3">
                           <div class="col-2 text-center ms-auto">
                             <a class="btn btn-link px-3" href="javascript:;">
                               <i class="fa fa-facebook text-white text-lg" aria-hidden="true"></i>
                             </a>
                           </div>
                           <div class="col-2 text-center px-1">
                             <a class="btn btn-link px-3" href="javascript:;">
                               <i class="fa fa-github text-white text-lg" aria-hidden="true"></i>
                             </a>
                           </div>
                           <div class="col-2 text-center me-auto">
                             <a class="btn btn-link px-3" href="javascript:;">
                               <i class="fa fa-google text-white text-lg" aria-hidden="true"></i>
                             </a>
                           </div>
                         </div>
                       </div>
                     </div>
                     <div class="card-body">


                    <Form
                        onSubmit={this.handleLogin}
                        ref={c => {
                            this.form = c;
                        }}
                    >
                      
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <Input
                                type="text"
                                className="form-control"
                                name="username"
                                value={this.state.username}
                                onChange={this.onChangeUsername}
                                maxlength="15"
                                validations={[required]}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <Input
                                type="password"
                                className="form-control"
                                name="password"
                                value={this.state.password}
                                onChange={this.onChangePassword}
                                validations={[required]}
                            />
                        </div>

                        <div className="form-group">
                            <button
                                 class="btn bg-gradient-primary w-100 my-4 mb-2"
                                disabled={this.state.loading}
                            >
                                {this.state.loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                )}
                                <span>Login</span>
                            </button>
                        </div>

                        {this.state.message && (
                            <div className="form-group">
                                <div className="alert alert-danger" role="alert">
                                    {this.state.message}
                                </div>
                            </div>
                        )}
                        <CheckButton
                            style={{ display: "none" }}
                            ref={c => {
                                this.checkBtn = c;
                            }}
                        />
                    </Form>
  <div class="card-footer text-center pt-0 px-lg-2 px-1">
                      <p class="mb-2 text-sm mx-auto">
                        Already have an account?
                        <a href="/register" class="text-primary text-gradient font-weight-bold">Sign up</a>
                      </p>
                    </div>
                     </div>
                   </div>

                 </div>
               </div>
             </div>
             <footer class="footer position-absolute bottom-2 py-2 w-100">
                     <div class="container">
                       <div class="row align-items-center justify-content-lg-between">
                         <div class="col-12 col-md-6 my-auto">
                           <div class="copyright text-center text-sm text-white text-lg-start">
                             © <script>
                               document.write(new Date().getFullYear())
                             </script>2022,
                             made with <i class="fa fa-heart" aria-hidden="true"></i> by
                             <a href="https://www.creative-tim.com" class="font-weight-bold text-white" target="_blank">Creative Tim</a>
                             for a better web.
                           </div>
                         </div>
                         <div class="col-12 col-md-6">
                           <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                             <li class="nav-item">
                               <a href="https://www.creative-tim.com" class="nav-link text-white" target="_blank">Creative Tim</a>
                             </li>
                             <li class="nav-item">
                               <a href="https://www.creative-tim.com/presentation" class="nav-link text-white" target="_blank">About Us</a>
                             </li>
                             <li class="nav-item">
                               <a href="https://www.creative-tim.com/blog" class="nav-link text-white" target="_blank">Blog</a>
                             </li>
                             <li class="nav-item">
                               <a href="https://www.creative-tim.com/license" class="nav-link pe-0 text-white" target="_blank">License</a>
                             </li>
                           </ul>
                         </div>
                       </div>
                     </div>
                   </footer>
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
       </main>
       </body>
     </div>

        );
    }
}
