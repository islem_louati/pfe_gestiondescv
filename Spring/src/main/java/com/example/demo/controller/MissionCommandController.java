package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commands.service.CandidatCommandService;
import com.example.demo.commands.service.MissionCommandService;
import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.Commentaire;
import com.example.demo.data.CreateCandidatRequest;
import com.example.demo.data.CreateMissionRequest;
import com.example.demo.data.Mission;
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/missions")
public class MissionCommandController {
	private final CandidatRepository rep;
	private final MissionCommandService missionCommandService ;

	public MissionCommandController(MissionCommandService missionCommandService, CandidatRepository rep) {
		this.rep = rep;
		this.missionCommandService = missionCommandService;
	}
	@PostMapping("/create")
	public ResponseEntity<String> createAccount(@RequestBody Mission request) {
		try {
			Mission response = missionCommandService.createAccount(request);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	@PutMapping("/update/{missionId}")
	public ResponseEntity<String> updateMission(@RequestBody Mission mission , @PathVariable int missionId) {

		try {
			missionCommandService.updateMission(mission,missionId);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
	@DeleteMapping("/delete/{missionId}")
	public ResponseEntity<String> deleteMission(@PathVariable int missionId) {

		try {
			missionCommandService.deletemission(missionId);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
	@PostMapping("/dessafec/{missionId}")
	public ResponseEntity<String> dessafect(@PathVariable int missionId) {
		try {
			Mission response = missionCommandService.DesaffectcandidatFromMission(missionId);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
