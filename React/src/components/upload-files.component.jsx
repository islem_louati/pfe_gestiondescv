import React, { Component } from "react";
import uploadFilesService from "../services/upload-files.service";


export default class UploadFiles extends Component {
  
  constructor(props) {
    super(props);
    this.selectFile = this.selectFile.bind(this);
    this.upload = this.upload.bind(this);

    this.state = {
      id: this.props.match.params.id,
      selectedFiles: undefined,
      currentFile: undefined,
      progress: 0,
      message: "",
      candidat:{},

      fileInfos: [],
    };
  }




  selectFile(event) {
    this.setState({
      selectedFiles: event.target.files,
    });
  }


  componentDidMount(){
    uploadFilesService.getEmployeeById(this.state.id).then( (res)=>{
        
        let currentFile = this.state.selectedFiles[0];
console.log(this.candidats.id+"nop")
        this.setState({
          progress: 0,
          currentFile: currentFile,
        });
        });
    
}
viewpdf(filename) {
  {
      
    this.props.history.push(`/pdf/${filename}`);
}}


  upload() { 
 
   
    let currentFile = this.state.selectedFiles[0];
const reader= new FileReader();
reader.readAsText(currentFile);
reader.onload=() =>{
  this.setState({fileName: currentFile.name , fileContent: reader.result});
  console.log(reader.result)
}
    this.setState({
      progress: 0,
      currentFile: currentFile,
    });

    
   

    uploadFilesService.upload(currentFile,this.state.id,  (event) => {
     
      this.setState({
        progress: Math.round((100 * event.loaded) / event.total),
      });
    })
      .then((response) => {
        this.setState({
          message: response.data.message,
        });
        this.props.history.push(`/candidat`);
      })
      .then((files) => {
        this.setState({
          fileInfos: files.data,
        });
      })
      .catch(() => {
        this.setState({
          progress: 0,
          message: "Could not upload the file!",
          currentFile: undefined,
        });
      });

    this.setState({
      selectedFiles: undefined,
    });
   
  }

  render() {
    const {
      selectedFiles,
      currentFile,
      progress,
      message,
      fileInfos,
    } = this.state;

    return (
    
        <div>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
          {currentFile && (
              <div className="progress">
                <div
                    className="progress-bar progress-bar-info progress-bar-striped"
                    role="progressbar"
                    aria-valuenow={progress}
                    aria-valuemin="0"
                    aria-valuemax="100"
                    style={{ width: progress + "%" }}
                >
                  {progress}%
                </div>
              </div>
          )}

          <label className="btn btn-default">
            <input type="file" onChange={this.selectFile} />
          </label>

          <button
              className="btn btn-success"
              disabled={!selectedFiles}
              onClick={this.upload}
          >
            Upload
          </button>
        

          <div className="alert alert-light" role="alert">
            {message}
          </div>

          <div className="card">
            <div className="card-header">List of Files</div>
            <ul className="list-group list-group-flush">
              {fileInfos &&
                  fileInfos.map((file, index) => (
                      <li className="list-group-item" key={index}>
                        <button  onClick={this.upload}>{file.name}</button>
                      </li>
                  ))}
            </ul>
          </div>
        </div>
    );
  }
}
