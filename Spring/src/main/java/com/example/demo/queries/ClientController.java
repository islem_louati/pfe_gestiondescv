package com.example.demo.queries;

import java.util.List;
import java.util.Set;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.Client;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/client")

public class ClientController {

	private final ClientServiceQuery c;

	public ClientController(ClientServiceQuery c) {
		this.c = c;
	}

	@GetMapping("/clients")
    //@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale')")
	public List<Client> getSummary() {
		return this.c.getallClinet();
	}
	@GetMapping("/clientsaveccome")
    //@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale')")
	public Set<Client> getclientavec() {
		return this.c.getclientaveccommerciale();
	}
	@GetMapping("/clientsanscome")
    //@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_Comerciale')")
	public Set<Client> getclientsans() {
		return this.c.getclientsanscommerciale();
	}


}
