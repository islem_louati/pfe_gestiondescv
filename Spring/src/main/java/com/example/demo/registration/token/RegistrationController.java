package com.example.demo.registration.token;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.commands.service.AppUserService;
import com.example.demo.commands.service.UserDetailsImpl;
import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.AppUserRole;
import com.example.demo.data.LoginRequest;
import com.example.demo.data.PasswordResetToken;
import com.example.demo.email.EmailSender;

import com.example.demo.exception.ResponseMessage;
import com.example.demo.security.config.JwtResponse;
import com.example.demo.security.config.JwtUtils;
import com.example.demo.util.GenericResponse;

@RestController

@RequestMapping(path = "api/v1/registration")
@CrossOrigin(origins = "http://localhost:3000")
public class RegistrationController {
	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	AppUserRepository appUserRepository;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private AppUserService aa;
	@Autowired
	private RegistrationService service;
	@Autowired
	private MessageSource messages;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	private Environment env;
	private final RegistrationService registrationService;
	private final AppUserRepository userrepo;

	public RegistrationController(RegistrationService registrationService, AppUserRepository userrepo) {
		this.registrationService = registrationService;
		this.userrepo = userrepo;
	}

	@PostMapping("/signinn")
	public ResponseEntity<?> authenticateUser(Boolean enabled ,@Valid @RequestBody LoginRequest loginRequest) {
		String message = "";

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		 userDetails.getAuthorities();
		
		System.out.println(userDetails + "aaa");
		for (AppUser aa : userrepo.findByenabled(enabled.TRUE))
			
			if (userDetails.getEmail().equals(aa.getEmail())) {
				try {
					System.out.println(aa + "aaa");
					return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
							userDetails.getEmail(), userDetails.isEnabled(), userDetails.getAppUserRole()));
	
					
				} catch (Exception e) {

				}
			}
		message = "you must confirmed your account: " + userDetails.getUsername();
		return new ResponseEntity<>("you must confirmed our account", HttpStatus.CONFLICT);
	
	}
	
	@PostMapping("/register/{userId}")
	public ResponseEntity<String> register(@RequestBody requestr request, @PathVariable int userId) {
		boolean userExists = appUserRepository.getByEmail(request.getEmail()).isPresent();
		boolean userExistss = appUserRepository.getByUsername(request.getUsername()).isPresent();
		String message = "";
		if ((userExistss )) {

			message = "Username already taken " + request.getUsername();
			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		} 
		if (( userExists)) {

			message = "emailll already taken " + request.getEmail();
			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		} 
		
	
		 if (request.getAppUserRole().equals(AppUserRole.COMMERCIALE)) {
				registrationService.registercomerciale(request, userId);
				return new ResponseEntity<>("", HttpStatus.OK);

			}
		 
		
			return new ResponseEntity<>( HttpStatus.ACCEPTED);
	}

	@PostMapping("/registercandidat/{candidatId}")
	public ResponseEntity<String> registercandidatfromadmin(@RequestBody requestr request, @PathVariable int candidatId) {
		boolean userExists = appUserRepository.getByEmail(request.getEmail()).isPresent();
		boolean userExistss = appUserRepository.getByUsername(request.getUsername()).isPresent();
		String message = "";
		if ((userExistss )) {

			message = "Username already taken " + request.getUsername();
			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		} 
		if (( userExists)) {

			message = "emailll already taken " + request.getEmail();
			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		} 
		
	
		 if (request.getAppUserRole().equals(AppUserRole.COMMERCIALE)) {
				registrationService.registercandidat(request, candidatId);
				return new ResponseEntity<>("", HttpStatus.OK);

			}
		 
		
			return new ResponseEntity<>( HttpStatus.ACCEPTED);
	}

	@PostMapping
	public ResponseEntity<String> register(@RequestBody RegistrationRequest request) {
		boolean userExists = appUserRepository.getByEmail(request.getEmail()).isPresent();
		boolean userExistss = appUserRepository.getByUsername(request.getUsername()).isPresent();
		String message = "";
		if ((userExistss )) {

			message = "Username already taken " + request.getUsername();
			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		} 
		if (( userExists)) {

			message = "emailll already taken " + request.getEmail();
			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		} 
		
		 if (request.getAppUserRole().equals(AppUserRole.ADMIN)) {
			registrationService.register(request);
			return new ResponseEntity<>("registration acount sucess please confirmed ur account", HttpStatus.OK);
		}
		// TODO check of attributes are the same and
		// TODO if email not confirmed send confirmation email.
		 if (request.getAppUserRole().equals(AppUserRole.CANDIDAT)) {
			registrationService.registerUser(request);
			return new ResponseEntity<>("registration sucess please waiting admin to confirm ur account", HttpStatus.OK);

		}
		 if (request.getAppUserRole().equals(AppUserRole.COMMERCIALE)) {
				registrationService.registerUser(request);
				return new ResponseEntity<>("registration sucess please waiting admin to confirm ur account", HttpStatus.OK);

			}
		 
		
			return new ResponseEntity<>( HttpStatus.ACCEPTED);
	}
@PostMapping("/valider/{id}")
public void valider(Boolean enabled,@PathVariable Long id)
{
	AppUser aa = userrepo.findById(id).orElse(null);
			aa.getEnabled();
			System.out.println(aa.getEnabled());
			aa.setEnabled(enabled.TRUE);
			userrepo.save(aa);
}
	@GetMapping(path = "confirm")
	public String confirm(@RequestParam("token") String token) {
		return registrationService.confirmToken(token);
	}

	@GetMapping("/enable")
	public List<AppUser> getenable(Boolean enabled) {
		return this.userrepo.findByenabled(enabled.TRUE);
	}
	@GetMapping("/nonenable")
	public List<AppUser> getnonenable(Boolean enabled) {
		return this.userrepo.findByenabled(enabled.FALSE);
	}

	@GetMapping("/users")
	public List<AppUser> getSummary() {
		return this.userrepo.findAll();
	}
	@GetMapping("/email/{id}")
	public void email(@PathVariable long id) {
		 registrationService.geybyid(id);
	}
	@GetMapping("/emailtocandidat/{id}")
	public void emailtocandidat(@PathVariable long id) {
		 registrationService.sendmailtocandidat(id);
	}
	@GetMapping("/usersbyname/{id}")
	public Optional<AppUser> getbyname(@PathVariable Long id) {
		AppUser a= new AppUser();
		a.getUsername();
	    Optional<AppUser> user = userrepo.findById(id);
		System.out.println(user);
		return  user;
	}
	// Reset password

}