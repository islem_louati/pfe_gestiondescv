package com.example.demo.controller;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commands.CreateClientCommand;
import com.example.demo.commands.service.ClientService;
import com.example.demo.data.Client;
import com.example.demo.data.comerciale;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/cliens")
public class ClientCommandController {

	public final ClientService cl;

	public ClientCommandController(ClientService cl) {
		this.cl = cl;
	}

	@PostMapping("/create")
   

	public ResponseEntity<String> createAccount(@RequestBody Client request ) {
		try {
			Client response = cl.createClient(request);

			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
	@PostMapping("/affect/{clientId}/{userId}")
	public ResponseEntity<String> affectAccountcomercialeToClient(@PathVariable int clientId,@PathVariable int userId ) {
		try {
			 cl.AffectCommercialeToClient(clientId,userId);

			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}

	@PutMapping("/update/{clientId}")
	   

	public ResponseEntity<String> updateAccountclient(@RequestBody Client request,@PathVariable int clientId ) {
		try {
			 cl.updateClient(request,clientId);

			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}
	@DeleteMapping("/delete/{clientId}")
	   

	public ResponseEntity<String> deleteAccountclient(@PathVariable int clientId ) {
		try {
			 cl.deleteClient(clientId);

			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}}

    @GetMapping("/client/{id}")
    
    public Optional<Client> getCandidat(@PathVariable int id) {
    	 return this.cl.getClientById(id);
    }
}
