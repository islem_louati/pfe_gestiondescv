package com.example.demo.commands;


import org.springframework.stereotype.Service;

import com.example.demo.data.QuestionEntity;
import com.example.demo.data.dto.AnswerDto;
import com.example.demo.data.dto.QuestionDto;

import java.util.Set;

public interface QuestionService {

	QuestionEntity createQuestion(QuestionEntity questionDto);
    Set<QuestionDto> getAllQuestions();
    QuestionDto getQuestionById(long id);
    QuestionDto updateAnswer(AnswerDto answerDto, long id);
    String deleteQuestionById(long id);
}
