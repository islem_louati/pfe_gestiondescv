package com.example.demo.queries;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import com.example.demo.data.Client;
import com.example.demo.data.ClientRepository;
import com.example.demo.data.comerciale;

@Service

public class ClientServiceQuery {
	private final ClientRepository cl;

	public ClientServiceQuery(ClientRepository cl) {
		this.cl = cl;
	
	}

	@QueryHandler
	public Client handle(Client query) {
		// log.info("Handling ...");
		Client client = cl.findById(query.getClienId()).orElse(null);

		return client;
	}



	@QueryHandler
	public Set<Client> getclientsanscommerciale() {
		 
			 Set<Client> clientList = new HashSet<>();
			
			for (Client cc : cl.findAll()) {
				if(cc.getComerciale() == null )
					
				
					clientList.add(cc);
			}
			return clientList;
		 

	}
	@QueryHandler
	public Set<Client> getclientaveccommerciale() {
		 
			 Set<Client> clientList = new HashSet<>();
			
			for (Client cc : cl.findAll()) {
				if(cc.getComerciale() != null )
					
				
					clientList.add(cc);
			}
			return clientList;
		 

	}
	@QueryHandler
	public List<Client> getallClinet() {

		return cl.findAll();

	}

}
