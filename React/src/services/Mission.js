import axios from 'axios';
import authHeader from './auth-header';


const  ALLMission_API_BASE_URL = "http://localhost:8087/SpringMVC/mission/allmission";
const  MissionTerminer_API_BASE_URL = "http://localhost:8087/SpringMVC/mission/missiontermine";
const  MissionsEnCours_API_BASE_URL = "http://localhost:8087/SpringMVC/mission/missionencour";
const  MissionsNonAffecter_API_BASE_URL = "http://localhost:8087/SpringMVC/mission/missionnonaffecte";
const  Missionsupdate_API_BASE_URL = "http://localhost:8087/SpringMVC/missions/update";

const  Missions_API_BASE = "http://localhost:8087/SpringMVC/mission/getmission";
const  MissionCREATE_API_BASE_URL = "http://localhost:8087/SpringMVC/missions/create";
const Affect_Mission_To_Candidat = "http://localhost:8087/SpringMVC/candidats/affect"
class Mission {
    getallmission() {
        return axios.get(ALLMission_API_BASE_URL,{ headers: authHeader() });
    }
    getmissionNonAffecter() {
        return axios.get(MissionsNonAffecter_API_BASE_URL,{ headers: authHeader() });
    }
    getmissionEnCours() {
        return axios.get(MissionsEnCours_API_BASE_URL,{ headers: authHeader() });
    }
    getmissionterminer() {
        return axios.get(MissionTerminer_API_BASE_URL,{ headers: authHeader() });
    }
    getmissionsById(missionId) {
        console.log(missionId)
        return axios.get(Missions_API_BASE+'/'+missionId,{ headers: authHeader() });
       
    }
    createMission(mission){
        return axios.post(MissionCREATE_API_BASE_URL, mission,{ headers: authHeader() });
    }
    updtaeMission(mission,missionId){
        return axios.put(Missionsupdate_API_BASE_URL+'/'+missionId, mission,{ headers: authHeader() });
    }
    affectMission(missionId,candidatId){
        console.log(missionId,candidatId)
        return axios.post(Affect_Mission_To_Candidat+'/'+missionId+'/'+candidatId,{ headers: authHeader() });
    }
}
export default  new Mission()