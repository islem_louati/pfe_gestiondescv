package com.example.demo.data;

public enum AppUserRole {
    CANDIDAT,
    ADMIN,
    COMMERCIALE
}
