package com.example.demo.commands.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.Candidat;
import com.example.demo.data.Client;
import com.example.demo.data.ClientRepository;
import com.example.demo.data.CommercialeRepository;
import com.example.demo.data.FileDB;
import com.example.demo.data.comerciale;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@RequiredArgsConstructor
@Component
public class ClientService {
	private final ClientRepository repository;
@Autowired
private CommercialeRepository repcom;
	private final AppUserRepository rep;
//	private final RoleRepository role;
	public ClientService(ClientRepository repository,AppUserRepository rep) {
		this.repository = repository;
		this.rep=rep;
//		this.role=role;
	}
	 public Client createClient(Client createClientRequest) {
	

    			 createClientRequest.getClienId();
    			 createClientRequest.getNom();
    			 createClientRequest.getNumerotel();
    			 createClientRequest.getDescription();
    			 createClientRequest.getAdresse();
    			 createClientRequest.getCodepostal();
    			 createClientRequest.getVille();
    			 createClientRequest.getPays();
    			 createClientRequest.getContactname();
    			 createClientRequest.getContactprenom();
    			 createClientRequest.getContacttel();
    		
    			
    	 
    	return this.repository.save(createClientRequest);
		
    }
	    public Client updateClient(Client createAccountRequest ,int ClienId) {
	    	
	    	Client client =	repository.findById(ClienId).orElse(null);
	    	comerciale c =client.getComerciale();
	    	if (client!=null)
	    	
	    			createAccountRequest.setClienId(ClienId);
	    	createAccountRequest.setComerciale(c);
	       return this.repository.save(createAccountRequest);
			
	    }
    public void deleteClient(int ClienId) {
	    	
	    	Client client =	repository.findById(ClienId).orElse(null);
	    	comerciale c =client.getComerciale();
	    	if (client!=null)
	    	
	    		
	        this.repository.deleteById(ClienId);
			
	    }
    public Optional<Client> getClientById(int ClienId) {
    	
    	
		return 	this.repository.findById(ClienId);
		
		
	
	}
    public void AffectCommercialeToClient(int ClienId, int userId) {
    	
    	
    		Client client =	repository.findById(ClienId).orElse(null);
    		comerciale com =repcom.findById(userId).orElse(null); 
    		
    		client.setComerciale(com);
    		repository.save(client);
    		
    	
    	}
    
}
