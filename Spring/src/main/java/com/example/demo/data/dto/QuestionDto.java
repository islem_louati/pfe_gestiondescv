package com.example.demo.data.dto;

import java.util.List;
import java.util.Set;

public class QuestionDto {

    private String Id;
    private String questionContent;
    private List<AnswerDto> answers;



    public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

	public List<AnswerDto> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerDto> answers) {
		this.answers = answers;
	}


}
