package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.example.demo.commands.service.CandidatCommandService;
import com.example.demo.commands.service.FilesStorageService;
import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.FileDB;
import com.example.demo.data.FileDBRepository;
import com.example.demo.data.FileInfo;
import com.example.demo.data.Mission;
import com.example.demo.data.Missionepository;
import com.example.demo.exception.ResponseMessage;

@RestController
@CrossOrigin(origins = "http://localhost:3000")

public class FilesController {
	@Autowired
	FilesStorageService storageService;
	@Autowired
	FileDBRepository filerepo;
	@Autowired
	private Missionepository rep;
	@Autowired
	private CandidatRepository can;
	@Autowired
	CandidatCommandService cmd;
	private static final String MP4_CONTENT_TYPE = "video/mp4";
	private static final String DOCX_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	private static final String PDF_CONTENT_TYPE = "application/pdf";
	private static final String IMG_CONTENT_TYPE = "image/jpg";
	
	@DeleteMapping("/deletefile/{CandidatId}/{id}")
public void search (@PathVariable int CandidatId,@PathVariable int id ) {
		 File dir = new File("uploads/files");
			File[] files = dir.listFiles();	 
	Candidat candidat =can.findById(CandidatId).orElse(null);
	candidat.setdBFile(null);
	can.save(candidat);
	FileDB fo = filerepo.findById(id).orElse(null);
	for (FileDB ff : filerepo.findAll()) {
					for (File file : files) {
						filerepo.delete(fo);
						if (ff.getName().equals(file.getName())) 
						{
							FileSystemUtils.deleteRecursively(file);
						
						
							
						}
			
						
			
		 		 }}
					
				
				}

	 		 
	@PostMapping("/upload/{CandidatId}")
	public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file,
			@PathVariable int CandidatId) throws IOException {

		boolean fileExists = filerepo.findByname(file.getName()).isPresent();
		String message = "";
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());
		System.out.println("rrrr" + FileDB.getType());

		if (!fileExists) {
			if (FileDB.getType().equals(PDF_CONTENT_TYPE) || FileDB.getType().equals(DOCX_CONTENT_TYPE)) {

				System.out.println("iss" + FileDB.getType());
				try {
					storageService.save(file);
					storageService.store(file, CandidatId);

					message = "Uploaded the file successfully: " + file.getOriginalFilename();
					return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ResponseMessage(message));

				}

				catch (Exception e) {

				}
			}
			message = "le nom du cv est existe modifier votre nom du cv: " + file.getOriginalFilename();
			return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(new ResponseMessage(message));
		}

		System.out.println("iss" + FileDB.getType());
		message = "Could not upload this type of file : " + file.getOriginalFilename() + "!";
		return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(new ResponseMessage(message));

	}
	@GetMapping("/allfiles/{missionId}")
	public Set<Candidat> getListFile(@PathVariable int missionId) throws IOException {
		Set<Candidat> fileList = new HashSet<>();
		
		String l = "islem";
		String message = "";
		String s = "";
		Mission p = this.rep.findById(missionId).orElse(null);
		p.getDescription();
		System.out.println(p.getDescription());
		boolean found = true;
		File dir = new File("uploads/files");
		File[] files = dir.listFiles();
		PDDocument document = null;
		for (FileDB ff : filerepo.findAll()) {

		for (File file : files) {
			if (file.isFile()) {
			if (file.getName().equals(ff.getName())) {
				
					BufferedReader inputStream = null;
					String line;

					FileInputStream fis = new FileInputStream(file);
					document = PDDocument.load(fis);
					

					PDFTextStripper pdfTextStripper = new PDFTextStripper();
					pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire
														// document
					pdfTextStripper.getEndPage(); // comment this line if you want to read the entire document
					String docText = pdfTextStripper.getText(document);

					if (docText.contains(l)) {
				
						fileList.add(ff.getCandidat());
						System.out.println(ff.getName()+"aaa");
			System.out.println(file.getName().substring(0, file.getName().length() - 4));

					}
					document.close();}}
		}}
return fileList;
		}
	
	@GetMapping("/allfilesbysearch/{name}")
	public Set<Candidat> getListFilebysearch(@PathVariable String name) throws IOException {
		Set<Candidat> fileList = new HashSet<>();
		
		String l = "islem";
		String message = "";
		String s = "";
	
		
		boolean found = true;
		File dir = new File("uploads/files");
		File[] files = dir.listFiles();
		PDDocument document = null;
		for (FileDB ff : filerepo.findAll()) {

		for (File file : files) {
			if (file.isFile()) {
			if (file.getName().equals(ff.getName())) {
				
					BufferedReader inputStream = null;
					String line;

					FileInputStream fis = new FileInputStream(file);
					document = PDDocument.load(fis);
					

					PDFTextStripper pdfTextStripper = new PDFTextStripper();
					pdfTextStripper.setStartPage(1); // comment this line if you want to read the entire
														// document
					pdfTextStripper.getEndPage(); // comment this line if you want to read the entire document
					String docText = pdfTextStripper.getText(document);

					if (docText.contains(name)) {
				
						fileList.add(ff.getCandidat());
						System.out.println(ff.getName()+"aaa");
			System.out.println(file.getName().substring(0, file.getName().length() - 4));

					}
					document.close();}}
		}}
return fileList;
		}
	
	@GetMapping("/files")
	public ResponseEntity<List<FileInfo>> getListFiles() {

		List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
			String filename = path.getFileName().toString();
		
			String url = MvcUriComponentsBuilder
					.fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
			return new FileInfo(filename, url);
		}).collect(Collectors.toList());

		return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
	}
	@GetMapping("/filesss")
	public List<FileDB> getListFile() {
		return filerepo.findAll();
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) throws FileNotFoundException, IOException {
		Resource file = storageService.load(filename);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

}
