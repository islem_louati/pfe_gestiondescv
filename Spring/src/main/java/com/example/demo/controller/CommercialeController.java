package com.example.demo.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commands.service.CommercialeCommandService;
import com.example.demo.commands.service.FilesStorageService;
import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.AppUserRole;
import com.example.demo.data.Candidat;
import com.example.demo.data.Mission;
import com.example.demo.data.comerciale;
import com.example.demo.exception.ResponseMessage;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/commerciale")
public class CommercialeController {
	@Autowired
	CommercialeCommandService comservice;
	 @Autowired
	 private AppUserRepository userrep;
	@PostMapping("/create/{id}")
		public ResponseEntity<ResponseMessage> createAccount(@RequestBody comerciale request,@PathVariable Long id) {
		   AppUser a =userrep.findById(id).orElse(null);
		   String message="";
		   if(a.getAppUserRole().equals(AppUserRole.ADMIN))
		      {
		try {
			
				comerciale response = comservice.createAccountCommerciale(request,id);

				return new ResponseEntity<>( HttpStatus.CREATED);
			} catch (Exception e) {
				message="An error occurred";

				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseMessage(message));
			}
		
		      }
		   message = "user n'est pas commerciale : " ;
			return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(new ResponseMessage(message));
		   

		}
	@PostMapping("/affect/{id}")
	public ResponseEntity<String> afect(@PathVariable Long id, @RequestBody comerciale createAccountRequest) {
		try {
			comerciale response = comservice.affectcomercialeTouser(id,createAccountRequest);
			
			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	@PostMapping("/create")
	public ResponseEntity<ResponseMessage> createCommerciale(@RequestBody comerciale request) {
	   String message="";
	 
	try {
		
			 comservice.createCommercialeByAdmin(request);

			return new ResponseEntity<>( HttpStatus.CREATED);
		} catch (Exception e) {
			message="An error occurred";

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseMessage(message));
		}
	
	      }

	
	@GetMapping("/usercomeciale")
	public List<AppUser> finduser()
	{
		return comservice.getusercommerciale();
	}
	
	
	@GetMapping("/gettcom")
	public Set<comerciale> findcom()
	{
		return comservice.getcom();
	}
}
