import axios from "axios";

const API_URL = "http://localhost:8087/SpringMVC/api/v1/";
const API_URL_signin = "http://localhost:8087/SpringMVC/api/v1/registration/";

class AuthService {
  login(username, password) {
    return axios
      .post(API_URL_signin + "signinn", {
        username,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(username, email, password,appUserRole) {
    return axios.post(API_URL + "registration", {
      username,
      email,
      password,
      appUserRole
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }
}

export default new AuthService();
