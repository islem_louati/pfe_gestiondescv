import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../Service/auth.service";



const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

 class Login extends Component {
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);

        this.state = {
            username: "",
            password: "",
            loading: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    handleLogin(e) {
        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        //this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.login(this.state.username, this.state.password)


                      .then( () => {
                      this.props.history.push("/profile");
                        })
                        .catch(() => {
                          this.setState({
                            progress: 0,
                            message: "you must confirmed your account !",
                            currentFile: undefined,
                          });
                        },

                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        loading: false,
                        message: resMessage
                    });
                }

            );
        } else {
            this.setState({
                loading: false
            });
        }
    }

    render() {
        return (
        
           <div class="container">
               <div class="row">
                   <div class="col-md-6">


                    <Form
                        onSubmit={this.handleLogin}
                        ref={c => {
                            this.form = c;
                        }}
                    >
                         <div class="card">
                                    <div  class="box">
                                     <h1>Login</h1>
                                    <p class="text-muted"> Please enter your login and password!</p>
                                    <div>
                                 <input type="text" name="" placeholder="Username" value={this.state.username} onChange={this.onChangeUsername}
                                              validations={[required]}/>
                                              </div>
                                              <div>
                                 <input type="password" name="" placeholder="Password" value={this.state.password} onChange={this.onChangePassword}
                                            validations={[required]}/>
                                            </div>
                                    <a class="forgot text-muted" href="#">Forgot password?</a>


                                  {this.state.loading && (
                                    <span class="box"></span>
                                     )}

                                    <input type="submit"
                                   name="" value="login" disabled={this.state.loading}/>

</div>
                                    </div>


                        {this.state.message && (
                            <div className="form-group">
                                <div className="alert alert-danger" role="alert">
                                    {this.state.message}
                                </div>
                            </div>
                        )}
                        <CheckButton
                            style={{ display: "none" }}
                            ref={c => {
                                this.checkBtn = c;
                            }}
                        />
                    </Form>
                    </div>
                </div>
            </div>
        );
    }
}
export default Login;