import axios from 'axios';
import authHeader from './auth-header';


const  CANDIDAT_API_BASE_URL = "http://localhost:8087/SpringMVC/candidat/candidats";
const  CANDIDATCREATE_API_BASE_URL = "http://localhost:8087/SpringMVC/candidats/create";
const  CANDIDAT_UPDATE_API_BASE_URL = "http://localhost:8087/SpringMVC/candidats/update";
const  CANDIDAT_DELETE_API_BASE_URL = "http://localhost:8087/SpringMVC/candidats/delete";
const  CANDIDAT_DELETE_API_BASE = "http://localhost:8087/SpringMVC/allfiles";
const  CANDIDAT_exporter_API_BASE = "http://localhost:8087/SpringMVC/Comment/comm";


    class CandidatService{
        getcandidats(){
            return axios.get(CANDIDAT_API_BASE_URL , { headers: authHeader() });
        }
        getmissionsimilaire(missionId){
            return axios.get(CANDIDAT_DELETE_API_BASE+'/'+missionId , { headers: authHeader() });
        }
        createCandidat(candidat){
            return axios.post(CANDIDATCREATE_API_BASE_URL, {candidat},{ headers: authHeader() });

        }
        updateCandidat(candidat,candidatId){
            return axios.put(CANDIDAT_UPDATE_API_BASE_URL+'/'+candidatId, candidat,{ headers: authHeader() });

        }
        exporterComment(commentaireId,response){
            axios.get(CANDIDAT_exporter_API_BASE+'/'+commentaireId,{ headers: authHeader() })
          
          }
        deleteCandidat(candidatId){
            return axios.delete(CANDIDAT_DELETE_API_BASE_URL+'/'+candidatId,{ headers: authHeader() });

        }



}
export default new CandidatService()