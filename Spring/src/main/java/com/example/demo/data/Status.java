package com.example.demo.data;

public enum Status {
    JOIN,
    MESSAGE,
    LEAVE
}