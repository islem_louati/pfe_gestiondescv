import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import CandidatService from "../Service/CandidatService";


const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
const invalide = value => {
    if(! value.match(/^([a-zA-Z ]+)$/)) {
        return (
            <div className="alert alert-danger" role="alert">
                Pseudo invalide!
            </div>
        );
    }
};



const vusername = value => {
    if (value.length < 3 || value.length > 20 ) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

class Register extends Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeAdresse = this.onChangeAdresse.bind(this);
        this.onChangeEtat=this.onChangeEtat.bind(this);

        this.state = {
            id: this.props.match.params.id,
            fullName: '',
            date: '',
            tel: '',
            adresse:'',
            etatc:'',
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            fullName: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            date: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            tel: e.target.value
        });
    }
    onChangeAdresse(e) {
        this.setState({
            adresse: e.target.value
        });
    }
    onChangeEtat(e) {
        this.setState({
            etatc: e.target.value
        });
    }
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            CandidatService.getcandidats(this.state.id).then( (res) =>{
                let candidat = res.data;
                this.setState({fullName: candidat.fullName,
                    date: candidat.date,
                    tel: candidat.tel,
                    adresse: candidat.adresse,
                    etatc :candidat.etatc
                });
            });
        }
    }
    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        let candidat = {fullName: this.state.fullName, date: this.state.date,tel:this.state.tel,adresse: this.state.adresse, etatc:this.state.etatc};

        if (this.checkBtn.context._errors.length === 0) {

            CandidatService.createCandidat(candidat
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                    this.props.history.push("/candidat");
                    window.location.reload();
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }

    render() {
        return (
            <div className="col-md-12">
                <div className="card card-container">


                    <Form
                        onSubmit={this.handleRegister}
                        ref={c => {
                            this.form = c;
                        }}
                    >
                        {!this.state.successful && (
                            <div>
                                <div className="form-group">
                                    <label htmlFor="fullName">Username</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="fullName"
                                        value={this.state.fullName}
                                        onChange={this.onChangeUsername}
                                        validations={[required, vusername,invalide]}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="date">date</label>
                                    <Input
                                        type="date"
                                        className="form-control"
                                        name="date"
                                        value={this.state.date}
                                        min="2021-01-01" max="2040-12-31"
                                        onChange={this.onChangeEmail}
                                        validations={[required]}

                                    />
                                    <span className="validity"></span>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="tel">tel</label>
                                    <Input
                                        type="number"
                                        className="form-control"
                                        name="password"
                                        value={this.state.tel}
                                        onChange={this.onChangePassword}
                                        validations={[required]}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="adresse">adresse</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="adresse"
                                        value={this.state.adresse}
                                        onChange={this.onChangeAdresse}
                                        validations={[required,vusername,invalide]}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="etatc">Etat</label>
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="etatc"
                                        value={this.state.etatc}
                                        onChange={this.onChangeEtat}
                                        validations={[required]}
                                    />
                                </div>

                                <div className="form-group">
                                    <button className="btn btn-primary btn-block" onClick={this.handleRegister} >Sign Up</button>
                                </div>
                            </div>
                        )}

                        {this.state.message && (
                            <div className="form-group">
                                <div
                                    className={
                                        this.state.successful
                                            ? "alert alert-success"
                                            : "alert alert-danger"
                                    }
                                    role="alert"
                                >
                                    {this.state.message}
                                </div>
                            </div>
                        )}
                        <CheckButton
                            style={{ display: "none" }}
                            ref={c => {
                                this.checkBtn = c;
                            }}
                        />
                    </Form>
                </div>
            </div>
        );
    }
}
export default Register;
