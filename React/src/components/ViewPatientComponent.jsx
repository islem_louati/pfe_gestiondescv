import React, { Component } from 'react';

import uploadFilesService from "../Service/upload-files.service";
import { Link } from 'react-router-dom';
class ViewPatientComponent extends Component {
    constructor(props){
        super(props)

        this.state={
            id:this.props.match.params.id,
      
            candidat:{},
          

        }
    
    }
    editCandidat(id){
        
        this.props.history.push(`/upload/${id}`);
    }
 
    componentDidMount(){
        uploadFilesService.getEmployeeById(this.state.id).then(res=>{
            this.setState({candidat:res.data});
          
        })
       

    }



      render() {
 
    
        return (
            <div>
            <br></br>
            <div className = "card col-md-6 offset-md-3">
                <h3 className = "text-center">  Détails du Candidat</h3>
                <div>
            <br></br>
            <div className = "card col-md-6 offset-md-3">
                <h3 className = "text-center">  Détails du Patient</h3>
                <div className = "card-body">
                    <div className = "row">
                        <label> Nom du Candidat: </label>
                        <div> { this.state.candidat.fullName }</div>
                        </div>
                    <div className = "row">
                        <label> Adresse du Candidat: </label>
                        <div> { this.state.candidat.adresse}</div>
                        </div>

                        <div className = "row">
                        <label> numéro du téléphone du Candidat: </label>
                        <div> { this.state.candidat.tel}</div>
                        </div>
                        <div className = "row">
                        <label> date : </label>
                        <div> { this.state.candidat.date}</div>
                        </div>
                        <div className = "row">
                        <label> etat : </label>
                        <div> {this.state.candidat.etatc}</div>
                        </div>
                        <div className = "row">
                        <label> cv : </label>
                        { this.state.candidat.dBFile === undefined  ?
                         (
                         <div>aa</div> 
                         ):
                this.state.candidat.dBFile === null  ?
                <div> <Link onClick={ () => this.editCandidat(this.state.candidat.candidatId)}>affecter cv </Link></div> 
                :(<div> { this.state.candidat.dBFile.name}</div> )
}

                        </div>
                       
                        

                </div>

            </div>
        </div>

                </div>

            </div>
        
        );
      }
    }
    

export default ViewPatientComponent;