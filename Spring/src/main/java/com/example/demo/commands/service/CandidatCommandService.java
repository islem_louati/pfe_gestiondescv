package com.example.demo.commands.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.commands.CreateCandidatCommand;
import com.example.demo.data.AppUser;
import com.example.demo.data.AppUserRepository;
import com.example.demo.data.Candidat;
import com.example.demo.data.CandidatRepository;
import com.example.demo.data.Commentaire;
import com.example.demo.data.CreateCandidatRequest;
import com.example.demo.data.FileDB;
import com.example.demo.data.FileDBRepository;
import com.example.demo.data.Mission;
import com.example.demo.email.EmailSender;
import com.example.demo.event.CandidatCreatedEvent;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
//@RequiredArgsConstructor
@Component
public class CandidatCommandService {
	 @Autowired
	  private FileDBRepository fileDBRepository;
	 @Autowired
	  private AppUserRepository app;
	 @Autowired
	  private  EmailSender emailSender;
	private final CandidatRepository repository;
	private final FileDBRepository rep;
	public CandidatCommandService(CandidatRepository repository,FileDBRepository rep) {
		this.repository= repository;
		this.rep=rep;
	}
//	@EventHandler
//	public void on(CandidatCreatedEvent event) {
//	   // log.debug("Handling a Bank Account creation command {}", event.getId());
//	    Candidat candidat = new Candidat(
//	            event.getId(),
//	            event.getFullName(),
//	            event.getDate(),
//	            event.getAdresse(),
//	            event.getTel()
//	    );
//	    this.repository.save(candidat);
//	}
   // private final CommandGateway commandGateway;

//    public CandidatCommandService(CommandGateway commandGateway) {
//        this.commandGateway = commandGateway;
//    }

    public Candidat createAccount(Candidat createAccountRequest,Long id ) {
    	String pattern = "dd/MM/yyyy";
    	AppUser n= app.findById(id).orElse(null);
    	FileDB f= new FileDB();
    			createAccountRequest.getCandidatId();
                createAccountRequest.getFullName();
                createAccountRequest.getDate();
                createAccountRequest.getAdresse();
                createAccountRequest.getTel();
                createAccountRequest.getEtatc();
                createAccountRequest.setAppuser(n);
              n.setCandidat(createAccountRequest);
            //  app.save(n);
                
        
       return repository.save(createAccountRequest);
		
    }
    public Candidat createCandidat(Candidat createAccountRequest ) {
    
    
    			createAccountRequest.getCandidatId();
                createAccountRequest.getFullName();
                createAccountRequest.getDate();
                createAccountRequest.getEmail();
                createAccountRequest.getAdresse();
                createAccountRequest.getTel();
                createAccountRequest.getEtatc(); 
                String link = "http://localhost:3000/commercialetouser/" + createAccountRequest.getCandidatId();
	            emailSender.sendTocommerciale(
	            		createAccountRequest.getEmail(),
	            		buildEmailtocandidat(createAccountRequest.getFullName(), link));
       return this.repository.save(createAccountRequest);
		
    }
    private String buildEmailtocandidat(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> the admin has been add your profile to commerciale so  Please click on the below link to created an account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Created Now</a> </p></blockquote>\n  <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }
    public Candidat updateCandidat(Candidat createAccountRequest ,int CandidatId) {
    	
    	Candidat candidat =	repository.findById(CandidatId).orElse(null);
    	if (candidat!=null)
    	
    			createAccountRequest.setCandidatId(CandidatId);
    	createAccountRequest.getdBFile();
    	createAccountRequest.getCommentaire();
       return this.repository.save(createAccountRequest);
		
    }
    
 
    public void deleteCandidat(int CandidatId) {
    	Candidat com = repository.findById(CandidatId).orElse(null);
    
 
   if (com != null)
   {
	   
       this.repository.deleteById(CandidatId);
		
   }}
    public void AffectCvToCandidat(int CandidatId, int id) {
    	
	
		Candidat candidat =	repository.findById(CandidatId).orElse(null);
		FileDB file =rep.findById(id).orElse(null); 
		
		candidat.setdBFile(file);
		repository.save(candidat);
		
	
	}


}
