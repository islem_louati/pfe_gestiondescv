import axios from 'axios';
import React, {Component, useEffect} from 'react';
import { render } from 'react-dom';
import { Link, useHistory } from 'react-router-dom';
import authHeader from '../../services/auth-header';
import authService from '../../services/auth.service';
import userService from '../../services/user.service';

import './profile.css'



export default () => {
const history = useHistory()
const current= authService.getCurrentUser();
const [list,setList] =React.useState([]);
useEffect (() => 
{

axios.get(`http://localhost:8087/SpringMVC/api/v1/registration/usersbyname/${current.id}`,{ headers: authHeader() })
.then((response) => {
  setList(response.data);
  console.log(response.data)
})
},[]

)

 const aaaa =() =>{
    history.push(`/addcandidattouser/${current.id}`);
}
const upload =() =>{
    history.push(`/upload/${list.candidat.candidatId}`);
}


   
return (
<div>
<br/>
<br/>
<br/>


<a href="javascript:" id="return-to-top"><span class="fa fa-arrow-up" aria-hidden="true"></span></a>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light navbar-fixed-top" id="ftco-navbar">
<div class="container-fluid">

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
<span class="fa fa-bars"></span> Menu
</button>
<div class="collapse navbar-collapse" id="ftco-nav">
<ul class="navbar-nav m-auto">
<li class="nav-item active"><a href="/profilee" class="nav-link">profile</a></li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Page</a>
<div class="dropdown-menu" aria-labelledby="dropdown04">
  <a class="dropdown-item" href="#">Page 1</a>
<a class="dropdown-item" href="#">Page 2</a>
<a class="dropdown-item" href="#">Page 3</a>
<a class="dropdown-item" href="#">Page 4</a>
</div>
</li>
<li class="nav-item"><a href="/chat" class="nav-link">chat</a></li>
<li class="nav-item"><a href="#" class="nav-link">Blog</a></li>
<li class="nav-item"><a href="#" class="nav-link">Contact</a></li>
</ul>
</div>
</div>
</nav>
<section class="section about-section gray-bg" id="about">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-6">
                        <div class="about-text go-to">
                            <h3 class="dark-color">Profile</h3>
                            <h6 class="theme-color lead">{list.username}</h6>
                            <p>I <mark>design and develop</mark> services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface and meaningful interactions.</p>
                            <div class="row about-list">
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>Role</label>
                                        <p>   {list.appUserRole
          }</p>
                                    </div>
                                  
                                    <div class="media">
                                        <label>nom</label>
                                        { list.candidat === undefined  ? 
<Link onClick={ () => aaaa(list.userId)}>completer profile </Link> :list.candidat ===null ?
<Link onClick={ () => aaaa(list.userId)}>completer profile</Link> :<td>{list.candidat.fullName}</td>
}
                                    </div>

                                    <div class="media">
                                        <label>cv</label>
            
                                        { list.candidat === undefined  ? 
<Link onClick={ () => aaaa(list.userId)}>completer profile </Link> :list.candidat ===null ?
<Link onClick={ () => aaaa(list.userId)}>completer profile </Link> : list.candidat.dBFile === undefined  ?
   <div class="button-group">
   <div class="text">Download CV</div>
   <div class="icon-cv"><Link  onClick={ () => upload(list.candidat.candidatId)} alt="Marina Marques CV" target="_blank"><span class="fa fa-arrow-circle-down" aria-hidden="true"></span></Link></div>
 </div> :
                       list.candidat.dBFile ===null ?
                       <div class="button-group">
                       <div class="text">Download CV</div>
                       <div class="icon-cv"><Link  onClick={ () => upload(list.candidat.candidatId)} alt="Marina Marques CV" target="_blank"><span class="fa fa-arrow-circle-down" aria-hidden="true"></span></Link></div>
                     </div>
                    
                           :<td>{list.candidat.dBFile.name}</td>
}
                                    </div>

                                    
                                
                                </div>
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>E-mail</label>
                                        <p> {list.email}</p>
                                    </div>
                                    <div class="media">
                                        <label>Phone</label>
                                        <p>820-885-3321</p>
                                    </div>
                                    <div class="media">
                                        <label>Skype</label>
                                        <p>skype.0404</p>
                                    </div>
                                    <div class="media">
                                        <label>Freelance</label>
                                        <p>Available</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-avatar">
                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" title="" alt=""/>
                        </div>
                    </div>
                </div>
            
            </div>
        </section>
<div className="container">


</div>

</div>
);
}


             

          
              

  





