package com.example.demo.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.Collections;


@Entity
public class AppUser  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	@NotEmpty(message = "entrer votre Nom")
	@Size(min =3,max=30)
	
    private String username;


//	@NotNull
//	@Size(max=30)
//	@NotEmpty(message = "entrer votre email")
	@Email	
    private String email;
	//@NotEmpty(message = "entrer votre password")
//	@Size(min =8,max=80)
//	@Pattern(regexp = "^((?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])){4,12}$",
//    message = "password must contain atleast 1 uppercase, 1 lowercase, 1 special character and 1 digit ")
    private String password;
    @Enumerated(EnumType.STRING)
    private AppUserRole appUserRole;
    private Boolean locked = false;
    private Boolean enabled = false;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", referencedColumnName = "userId")
	private comerciale comerciale;
	public comerciale getComerciale() {
		return comerciale;
	}



	public AppUser(@NotEmpty(message = "entrer votre Nom") @Size(min = 3, max = 30) String username,
			@Email String email, String password, AppUserRole appUserRole, Boolean enabled, Candidat candidat) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.enabled = enabled;
		this.candidat = candidat;
	}



	public AppUser(@NotEmpty(message = "entrer votre Nom") @Size(min = 3, max = 30) String username,
			@Email String email, String password, AppUserRole appUserRole,
			comerciale comerciale) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.comerciale = comerciale;
	}



	public comerciale setComerciale(comerciale comerciale) {
		return this.comerciale = comerciale;
	}



	public Candidat getCandidat() {
		return candidat;
	}



	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}


	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CandidatId", referencedColumnName = "CandidatId")
	private Candidat candidat;
    public AppUser() {
		super();
	}



	@Override
	public String toString() {
		return "AppUser [username=" + username + ", email=" + email + ", appUserRole=" + appUserRole + ", enabled="
				+ enabled + "]";
	}



	public AppUser(String username, @Email String email, String password, AppUserRole appUserRole) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
	}



	public void setUsername(String username) {
		this.username = username;
	}



    public AppUser(@NotEmpty(message = "entrer votre Nom") @Size(min = 3, max = 30) String username,
			@Email String email, String password, AppUserRole appUserRole, Boolean enabled,
			com.example.demo.data.comerciale comerciale) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.enabled = enabled;
		this.comerciale = comerciale;
	}



	public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority =
                new SimpleGrantedAuthority(appUserRole.name());
        return Collections.singletonList(authority);
    }
    

    
    public AppUser(@NotEmpty(message = "entrer votre Nom") @Size(min = 3, max = 30) String username,
			@Email String email, String password, AppUserRole appUserRole, Boolean locked, Boolean enabled) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.appUserRole = appUserRole;
		this.locked = locked;
		this.enabled = enabled;
	}



	public String getPassword() {
        return password;
    }

    
    public String getUsername() {
        return username;
    }

 

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public AppUserRole getAppUserRole() {
		return appUserRole;
	}

	public void setAppUserRole(AppUserRole appUserRole) {
		this.appUserRole = appUserRole;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}


	public void setPassword(String password) {
		this.password = password;
	}

//	@Override
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return !locked;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return enabled;
//    }
}
