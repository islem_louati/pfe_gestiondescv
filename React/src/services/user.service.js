import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8087/SpringMVC/api/v1/registration/';
const API_URL_BYNAME = 'http://localhost:8087/SpringMVC/api/v1/registration/usersbyname';
const API_URL_all_user = 'http://localhost:8087/SpringMVC/api/v1/registration/';
const  CANDIDAT_API_BASE_URL = "http://localhost:8087/SpringMVC/candidat/";
const  CANDIDAT_API_BASE_URL_byfile = "http://localhost:8087/SpringMVC/candidats/candidatbyfile";
class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(CANDIDAT_API_BASE_URL + 'candidats', { headers: authHeader() });
  }

  getallUser() {
    return axios.get(API_URL_all_user + 'users', { headers: authHeader() });
  }
  getUserByname(id) {
  
    return axios.get(API_URL_BYNAME+'/'+id , { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(CANDIDAT_API_BASE_URL + 'candidats', { headers: authHeader() });
  }
  getcandidatbyfile() {
    return axios.get(CANDIDAT_API_BASE_URL_byfile , { headers: authHeader() });
  }
}

export default new UserService();
