package com.example.demo.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CommercialeRepository extends JpaRepository<comerciale, Integer> {

//	@Query("select c from comerciale c where c.appUser.appUserRole=:COMMERCIALE")
//	List<comerciale> getcomercialeByrole(@Param("COMMERCIALE") AppUserRole appUserRole);
}
