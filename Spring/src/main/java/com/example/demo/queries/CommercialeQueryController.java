package com.example.demo.queries;

import java.util.List;
import java.util.Optional;

import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.AppUser;
import com.example.demo.data.Candidat;
import com.example.demo.data.CommercialeRepository;
import com.example.demo.data.Mission;
import com.example.demo.data.comerciale;
@RestController
@RequestMapping(value = "/users")
@CrossOrigin(origins = "http://localhost:3000")
public class CommercialeQueryController {
	private final QueryGateway queryGateway;
	private final CommercialeQueryService m;
	@Autowired
	private CommercialeRepository com;

	public CommercialeQueryController(QueryGateway queryGateway,CommercialeQueryService m) {
		this.queryGateway = queryGateway;
		this.m= m;
	}

	 @GetMapping("/usercommerciale")
	   
	    public List<AppUser> getuserCommerciale(){
	        return this.m.getuserCommerciale();
	    }
	 @GetMapping("/commerciale")
	    public List<comerciale> getCommerciale(){
	        return this.com.findAll();
	    }
	    @GetMapping("/getcommerciale/{id}")
	    public Optional<comerciale> getCandidat(@PathVariable int id) {
	    	 return this.com.findById(id);
	    }
	 
}
