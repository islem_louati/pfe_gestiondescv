import React, {Component} from 'react';
import CommentService from '../services/CommentService';
import uploadFilesService from '../services/upload-files.service';



export default class CreateCommentComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            impression_sourcing: '',
            disposition_par_rapport_statutAE: '',
            niveaux_francais: '',
            disposition_negociation_salaire: '',
            dernier_poste:'',
            date_relance:'',
            salaire_negocie:'',
            candidat:''
        }
    }
    componentDidMount(){
        if(this.state.id === '_add'){
            return
        }else{
        uploadFilesService.getEmployeeById(this.state.id).then( (res)=>{
            
            let comment = res.data;
            this.setState({impression_sourcing: comment.impression_sourcing,
                disposition_par_rapport_statutAE: comment.disposition_par_rapport_statutAE,
                niveaux_francais: comment.niveaux_francais,
                disposition_negociation_salaire: comment.disposition_negociation_salaire,
                dernier_poste:comment.dernier_poste,
                date_relance:comment.date_relance,
                salaire_negocie:comment.salaire_negocie
            });
      
            });
        
    }
}
    changesalaire_negocieHandler= (event) => {
        this.setState({salaire_negocie: event.target.value});
    }
    changedernier_posteHandler= (event) => {
        this.setState({dernier_poste: event.target.value});
    }
    changedisposition_negociation_salaireHandler= (event) => {
        this.setState({disposition_negociation_salaire: event.target.value});
    }
    
    changeniveaux_francaisHandler= (event) => {
        this.setState({niveaux_francais: event.target.value});
    }
    
    changeimpression_sourcingHandler= (event) => {
        this.setState({impression_sourcing: event.target.value});
    }
    changedisposition_par_rapport_statutAEHandler= (event) => {
        this.setState({disposition_par_rapport_statutAE: event.target.value});
    }
  
    savecomment = (m) => {
        m.preventDefault();
        let comments = {impression_sourcing: this.state.impression_sourcing,
            disposition_par_rapport_statutAE: this.state.disposition_par_rapport_statutAE,
            niveaux_francais:this.state.niveaux_francais,disposition_negociation_salaire: this.state.disposition_negociation_salaire,
            dernier_poste: this.state.dernier_poste,date_relance: this.state.date_relance,salaire_negocie: this.state.salaire_negocie};
     
          

        // step 5

        CommentService.affectComment(comments,this.state.id)
        .then(res =>{
            this.props.history.push('/candidat');
        })
        .catch((error) => {
                // Error
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    // console.log(error.response.data);
                    // console.log(error.response.status);
                    // console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the
                    // browser and an instance of
                    // http.ClientRequest in node.js
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });



    }
    render() {
        return (
            <div>
                <br></br>
                <body>
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl bg-gradient-dark my-3 fixed-start ms-3  ps bg-white pg" id="sidenav-main" >
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <img src="./assest/logo-white.webp" class="navbar-brand-img h-100" alt="main_logo"/>
    </div>
    <hr class="horizontal light mt-0 mb-2"></hr>
    <div class="collapse navbar-collapse w-auto max-height-vh-100 ps ps--active-y" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active BgOfLink" href="/list">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Quiz</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/commerciale">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">comerciale</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/listnonenable">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">user non valider</span>
          </a>
        </li>


        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/candidat">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Candidat</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/client">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Client</span>
          </a>
        </li>
          <li class="nav-item">
                  <a class="nav-link text-white " href="/missions">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                      <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mission</span>
                  </a>
          </li>



        <li class="nav-item">
          <a class="nav-link text-white " href="/profile">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1" >profile</span>
          </a>
        </li>
      
      </ul>
    <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
    <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}>
    </div>
    </div>
    <div class="ps__rail-y" style={{top: '0px', height: '364px', right: '0px'}}>
    <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '270px'}}>
    </div>
    </div>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn BgOfLink mt-4 w-100" href="https://www.creative-tim.com/product/material-dashboard-pro?ref=sidebarfree" type="button"></a>
      </div>
    </div>
  <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
  <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
  </div>
  <div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
  <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div>
  </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ps ps--active-y">
   
       <div class="container-fluid py-4">
       
       </div>
       <div class="ps__rail-x" style={{left: '0px', bottom: '-2073px'}}>
       <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div></div>
       <div class="ps__rail-y" style={{top: '2073px', height: '724px', right: '0px'}}>
       <div class="ps__thumb-y" tabindex="0" style={{top: '536px', height: '187px'}}></div></div>
    </main>
    <div class="fixed-plugin ps">
   
        <div class="card shadow-lg">
          <div class="card-header pb-0 pt-3">
            <div class="float-start">
              <h5 class="mt-3 mb-0">Material UI Configurator</h5>
              <p>See our dashboard options.</p>
            </div>
            <div class="float-end mt-4">
              <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
                <i class="material-icons">clear</i>
              </button>
            </div>

          </div>
          <hr class="horizontal dark my-1"></hr>
          <div class="card-body pt-sm-3 pt-0">

            <div>
              <h6 class="mb-0">Sidebar Colors</h6>
            </div>
            <a href="javascript:void(0)" class="switch-trigger background-color">
              <div class="badge-colors my-2 text-start">
                <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
                <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
              </div>
            </a>

            <div class="mt-3">
              <h6 class="mb-0">Sidenav Type</h6>
              <p class="text-sm">Choose between 2 different sidenav types.</p>
            </div>
            <div class="d-flex">
              <button class="btn bg-gradient-dark px-3 mb-2 active disabled" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
              <button class="btn bg-gradient-dark px-3 mb-2 ms-2 disabled" data-class="bg-white" onclick="sidebarType(this)">White</button>
            </div>
            <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>

            <div class="mt-3 d-flex">
              <h6 class="mb-0">Navbar Fixed</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-3"></hr>
            <div class="mt-2 d-flex">
              <h6 class="mb-0">Light / Dark</h6>
              <div class="form-check form-switch ps-0 ms-auto my-auto">
                <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)"/>
              </div>
            </div>
            <hr class="horizontal dark my-sm-4"></hr>
            <a class="btn btn-outline-dark w-100" href="">View documentation</a>

            <div class="w-100 text-center">
              <span></span>
              <h6 class="mt-3">Thank you for sharing!</h6>
              <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
              </a>
              <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
                <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
              </a>

            </div>
          </div>
        </div>

      <div class="ps__rail-x" style={{left: '0px', bottom: '0px'}}>
      <div class="ps__thumb-x" tabindex="0" style={{left: '0px', width: '0px'}}></div>
      </div><div class="ps__rail-y" style={{top: '0px', right: '0px'}}>
      <div class="ps__thumb-y" tabindex="0" style={{top: '0px', height: '0px'}}></div></div></div>
    </body>
                <div className = "container">
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                           
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> impression_sourcing: </label>
                                        <select onChange={(ddl)=>{this.setState({impression_sourcing:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--impression_sourcing--</option>
                                                <option value="SERIEUX">SERIEUX</option>
                                                <option value="NON_SERIEUX">NON_SERIEUX</option>
                                                <option value="MOTIVE">MOTIVE</option>
                                                <option value="EN_FORMATION">EN_FORMATION</option>
                                                
                                            </select>
                                    </div>
                                    <div className = "form-group">
                                        <label> disposition_par_rapport_statutAE: </label>
                                        <select onChange={(ddl)=>{this.setState({disposition_par_rapport_statutAE:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--disposition_par_rapport_statutAE--</option>
                                                <option value="OUVERT">OUVERT</option>
                                                <option value="ACCEPTE">ACCEPTE</option>
                                            </select>
                                    </div>
                                    <div className = "form-group">
                                        <label> niveaux_francais: </label>
                                        <select onChange={(ddl)=>{this.setState({niveaux_francais:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--Etat--</option>
                                                <option value="COURANT">COURANT</option>
                                                <option value="MOYEN">MOYEN</option>
                                                <option value="MEDIOCRE">MEDIOCRE</option>
                                                <option value="ACCENT">ACCENT</option>
                                                <option value="NOTION">NOTION</option>
                                            </select>
                                    </div>

                                    <div className = "form-group">
                                        <label> disposition_negociation_salaire: </label>
                                        <select onChange={(ddl)=>{this.setState({disposition_negociation_salaire:ddl.target.value})}} >Etat
                                                <option disabled selected="True">--niveaux_francais--</option>
                                                <option value="RIGIDE">RIGIDE</option>
                                                <option value="SOUPLE">SOUPLE</option>
                                                <option value="HESITANT">HESITANT</option>
                                               
                                                
                                            </select>
                                    </div>
                                    <div className = "form-group">
                                        <label> dernier_poste: </label>
                                        <input placeholder="dernier_poste" name="dernier_poste" className="form-control"
                                               value={this.state.dernier_poste} onChange={this.changedernier_posteHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> date_relance: </label>
                                        <input placeholder="dernier_poste" name="date_relance" className="form-control" type="date"                                               value={this.state.date_relance} onChange={this.changedate_relanceHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> salaire_negocie: </label>
                                        <input placeholder="salaire_negocie" name="salaire_negocie" className="form-control"
                                               value={this.state.salaire_negocie} onChange={this.changesalaire_negocieHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.savecomment}>Save</button>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
    
}